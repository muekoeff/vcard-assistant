{
	"config": {
		"disableeditlinksifpresent": [
			"#Orte",
			"#Weitere_Ziele",
			"#Städte",
			"#Regionen",
			"#Inseln",
			"#print-districts"
		],
		"editlinkcontainer": "span.listing-metadata-items",
		"helpurl": "https://de.wikivoyage.org/wiki/Benutzer:Nw520/vCardAssistant.js",
		"outdatedwarning": true,
		"submitLegal": "Mit dem Speichern von Änderungen erklärst du dich mit den <a class=\"external\" target=\"_blank\" rel=\"noopener\" href=\"//wikimediafoundation.org/wiki/Terms_of_Use/de\">Nutzungsbedingungen</a> einverstanden und lizenzierst deine Bearbeitung unwiderruflich unter der Lizenz <a class=\"external\" target=\"_blank\" rel=\"noopener\" href=\"https://creativecommons.org/licenses/by-sa/3.0/deed.de\"><i>Creative-Commons</i> „Namensnennung – Weitergabe unter gleichen Bedingungen 3.0“</a> und der <a class=\"external\" target=\"_blank\" rel=\"noopener\" href=\"https://de.wikipedia.org/wiki/Wikipedia:GNU_Free_Documentation_License\">GFDL</a>. Du stimmst einer Autorennennung mindestens durch URL oder Verweis auf den Artikel zu. Wenn du nicht möchtest, dass dein Text weiterbearbeitet und weiterverbreitet wird, dann veröffentliche die Änderungen nicht.",
		"templateToType": {
			"vCard": ""
		},
		"title": "vCard-Assistant",
		"togglevalue_off": "n",
		"togglevalue_on": "y"
	},
	"special_parameters": {
		"lastupdate": "lastedit",
		"name": "name",
		"wikidata": "wikidata"
	},
	"parameters": {
		"order": [
			"name",
			"name-extra",
			"name-local",
			"name-latin",
			"alt",
			"wikidata",
			"auto",
			"type",
			"subtype",
			"comment",
			"address",
			"address-local",
			"directions",
			"directions-local",
			"lat",
			"long",
			"zoom",
			"url",
			"email",
			"phone",
			"fax",
			"tollfree",
			"mobile",
			"image",
			"commonscat",
			"facebook",
			"instagram",
			"twitter",
			"youtube",
			"flickr",
			"skype",
			"hours",
			"checkin",
			"checkout",
			"payment",
			"price",
			"before",
			"description",
			"lastedit",
			"show",
			"group",
			"map-group"
		],
		"categories": [
			{
				"category": {
					"id": "basic",
					"label": "Grundlegende Informationen"
				},
				"params": {
					"name": {
						"label": "Name",
						"description": "Genaue und vorzugsweise offizielle Bezeichnung der Einrichtung.",
						"type": "string",
						"required": true,
						"neverOmit": true
					},
					"name-extra": {
						"label": "Namensergänzung",
						"description": "Namensergänzung, die nicht Namensbestandteil ist. Dies können z.B. auch Symbolbilder sein.",
						"type": "string",
						"hidden": true
					},
					"name-local": {
						"label": "Name in Landessprache",
						"description": "Genaue Bezeichnung der Einrichtung in der Landessprache. Zusätzlich zum Namen.",
						"type": "string",
						"hidden": true,
						"wikidata": "P1705"
					},
					"name-latin": {
						"label": "Name in Umschrift",
						"description": "Genaue Bezeichnung der Einrichtung in der Umschrift der Landessprache.",
						"type": "string",
						"hidden": true
					},
					"alt": {
						"label": "Alternativname",
						"description": "Alternative Bezeichnung der Einrichtung.",
						"type": "string"
					},
					"comment": {
						"label": "Anmerkung",
						"description": "Anmerkung zum Namen oder zur Einrichtung, die nicht Namensbestandteil ist.",
						"type": "string",
						"hidden": true
					},
					"wikidata": {
						"label": "Wikidata-Datenobjekt",
						"description": "Kennung des entsprechenden Wikidata-Datenobjekts.",
						"type": "wikidata-item-id",
						"example": "Q82425",
						"neverOmit": true
					},
					"auto": {
						"label": "Automatischer Datenbezug von Wikidata",
						"description": "Aktiviert den automatischen Datenbezug aus Wikidata, soweit kein Wert für einen Parameter angegeben wird. Unterstützte Parameter sind im vCard-Assistenten mit einem entsprechenden Symbol gekennzeichnet. Durch Eingabe von \"n\" bei diesen Parametern kann ein automatischer Datenbezug von Wikidata für dieses Feld unterbunden werden.",
						"type": "toggle",
						"format": [
							{
								"regexp": "^(j|ja|y|yes|n|no|nein)$",
								"message": "Bitte gib entweder \"y\" für ja, oder \"n\" für nein ein."
							}
						],
						"triggers": [
							{
								"conditions": [
									{
										"sender": "wikidata",
										"value": "^$"
									}
								],
								"action": {
									"value": 0
								}
							},
							{
								"conditions": [
									{
										"sender": "wikidata",
										"value": "^$"
									}
								],
								"action": {
									"visible": false
								}
							},
							{
								"conditions": [
									{
										"sender": "wikidata",
										"value": ".+"
									}
								],
								"action": {
									"visible": true
								}
							},
							{
								"conditions": [
									{
										"sender": "wikidata",
										"value": "^$"
									}
								],
								"action": {
									"excluded": true
								}
							},
							{
								"conditions": [
									{
										"sender": "$this",
										"value": "^n$"
									}
								],
								"action": {
									"excluded": true
								}
							},
							{
								"conditions": [
									{
										"sender": "wikidata",
										"value": ".+"
									},
									{
										"sender": "$this",
										"value": "^y$"
									}
								],
								"action": {
									"excluded": false
								}
							}
						]
					},
					"type": {
						"label": "Typen",
						"description": "Die Typen sind von links nach rechts hierarchisch absteigend geordnet. Der erste Typ ist die Hauptkategorie.",
						"type": "enum",
						"enum": "type",
						"required": true,
						"multiple": true,
						"neverOmit": true
					},
					"subtype": {
						"label": "Features\/Untertypen",
						"description": "Weitere Spezifikation der Einrichtung. Die Reihenfolge der Features ist irrelevant.",
						"type": "enum",
						"enum": "subtype",
						"multiple": true
					},
					"description": {
						"label": "Beschreibung",
						"type": "string",
						"multiline": 5
					},
					"before": {
						"label": "Vorsatz",
						"description": "Beschreibung der Einrichtung, welche vor dem Namen angezeigt werden soll.",
						"type": "string",
						"hidden": true
					}
				}
			},
			{
				"category": {
					"id": "location",
					"label": "Ort und Lage"
				},
				"params": {
					"address": {
						"label": "Anschrift",
						"type": "string",
						"wikidata": "P6375"
					},
					"address-local": {
						"label": "Anschrift in Landessprache",
						"description": "Anschrift der Einrichtung in der Landessprache. Zusätzlich zur Anschrift.",
						"type": "string",
						"hidden": true
					},
					"directions": {
						"label": "Lage\/Beschreibung zur Anfahrt und zum Auffinden",
						"type": "string"
					},
					"directions-local": {
						"label": "Lage in Landessprache",
						"description": "Angaben zur Lage der Einrichtung in der Landessprache. Zusätzlich zur Lage.",
						"type": "string",
						"hidden": true
					},
					"lat": {
						"label": "Geografische Breite",
						"description": "Geografische Breite der Position der Einrichtung.",
						"type": "string",
						"example": "52.516272",
						"format": [
							{
								"regexp": "^-?[0-9]+(\\.[0-9]+)?$"
							}
						],
						"wikidata": {
							"property": "P625",
							"value": "latitude"
						},
						"neverOmit": true
					},
					"long": {
						"label": "Geografische Länge",
						"description": "Geografische Länge der Position der Einrichtung.",
						"type": "string",
						"example": "13.377722",
						"format": [
							{
								"regexp": "^-?[0-9]+(\\.[0-9]+)?$"
							}
						],
						"preview": {
							"type": "coordinates",
							"lat": "lat",
							"long": "$this"
						},
						"wikidata": {
							"property": "P625",
							"value": "longitude"
						},
						"neverOmit": true
					},
					"zoom": {
						"label": "Zoom-Level",
						"default": 17,
						"description": "Zoom-Level der darzustellen Karte zwischen 0 und 19. Standard ist 17. Eine Angabe ist normalerweise nicht notwendig.",
						"type": "number",
						"hidden": true,
						"max": 20,
						"min": 0
					}
				}
			},
			{
				"category": {
					"id": "contact",
					"label": "Kontaktinformationen"
				},
				"params": {
					"url": {
						"label": "URL",
						"type": "url",
						"example": "https:\/\/example.com\/",
						"wikidata": "P856"
					},
					"email": {
						"label": "E-Mail",
						"type": "string",
						"example": "sample@example.com",
						"wikidata": {
							"property": "P968",
							"filter": {
								"regexp": "mailto:(.+)",
								"group": 1
							}
						}
					},
					"phone": {
						"label": "Telefon",
						"type": "phone",
						"example": "+55 555 555-5555",
						"wikidata": "P1329"
					},
					"fax": {
						"label": "Fax",
						"type": "phone",
						"example": "+55 555 555-5555",
						"hidden": true,
						"wikidata": "P2900"
					},
					"tollfree": {
						"label": "Gebührenfreie Telefonnummern",
						"type": "string",
						"example": "+49 800 100-1000",
						"hidden": true
					},
					"mobile": {
						"label": "Mobil-Telefonnummer",
						"type": "string",
						"example": "+55 123 555-5555",
						"hidden": true
					}
				}
			},
			{
				"category": {
					"id": "networks",
					"label": "Netzwerke"
				},
				"params": {
					"commonscat": {
						"label": "Commons-Kategorie",
						"description": "Die Commons-Kategorie mit Medien zur Einrichtung sollte nur angegeben werden, wenn es keinen Wikidata-Eintrag gibt.",
						"type": "commons-category",
						"hidden": true,
						"wikidata": "P373"
					},
					"facebook": {
						"label": "Facebook",
						"type": "string",
						"wikidata": "P2013",
						"preview": {
							"type": "url",
							"url": [
								{
									"conditions": [
										{
											"sender": "$this",
											"value": "^http(s)?:"
										}
									],
									"url": "{parameter}"
								}, {
									"url": "https:\/\/facebook.com\/{parameter}"
								}
							]
						}
					},
					"flickr": {
						"label": "Flickr",
						"type": "string",
						"hidden": true,
						"wikidata": "P3267",
						"preview": {
							"type": "url",
							"url": [
								{
									"conditions": [
										{
											"sender": "$this",
											"value": "^http(s)?:"
										}
									],
									"url": "{parameter}"
								}, {
									"url": "https:\/\/flickr.com\/photos\/{parameter}"
								}
							]
						}
					},
					"instagram": {
						"label": "Instagram",
						"type": "string",
						"wikidata": "P2003",
						"preview": {
							"type": "url",
							"url": [
								{
									"conditions": [
										{
											"sender": "$this",
											"value": "^http(s)?:"
										}
									],
									"url": "{parameter}"
								}, {
									"url": "https:\/\/instagram.com\/{parameter}"
								}
							]
						}
					},
					"twitter": {
						"label": "Twitter",
						"type": "string",
						"wikidata": "P2002",
						"preview": {
							"type": "url",
							"url": [
								{
									"conditions": [
										{
											"sender": "$this",
											"value": "^http(s)?:"
										}
									],
									"url": "{parameter}"
								}, {
									"url": "https:\/\/twitter.com\/{parameter}"
								}
							]
						}
					},
					"youtube": {
						"label": "YouTube",
						"type": "string",
						"hidden": true,
						"wikidata": "P2397",
						"preview": {
							"type": "url",
							"url": [
								{
									"conditions": [
										{
											"sender": "$this",
											"value": "^http(s)?:"
										}
									],
									"url": "{parameter}"
								}, {
									"url": "https:\/\/youtube.com\/channel\/{parameter}"
								}
							]
						}
					},
					"skype": {
						"label": "Skype",
						"type": "string",
						"hidden": true,
						"wikidata": "P2893"
					}
				}
			},
			{
				"category": {
					"id": "details",
					"label": "Details"
				},
				"params": {
					"hours": {
						"label": "Öffnungszeiten",
						"type": "string",
						"example": "Mo–Fr 9:00–18:00"
					},
					"price": {
						"label": "Preise",
						"description": "Eintritts- oder Übernachtungspreise der Einrichtung.",
						"type": "string"
					},
					"payment": {
						"label": "Zahlungsarten",
						"description": "Von der Einrichtung akzeptierte Zahlungsarten.",
						"type": "string",
						"example": "Mastercard, Visa, Amex"
					},
					"checkin": {
						"label": "Check-in",
						"description": "Frühester Check-in der Einrichtung.",
						"type": "string",
						"hidden": true
					},
					"checkout": {
						"label": "Check-out",
						"description": "Spätester Check-out der Einrichtung.",
						"type": "string",
						"hidden": true
					},
					"image": {
						"label": "Bild zum POI",
						"type": "commons-file",
						"wikidata": "P18"
					}
				}
			},
			{
				"category": {
					"id": "advanced",
					"label": "Erweitert"
				},
				"params": {
					"lastedit": {
						"label": "Letzte Aktualisierung",
						"description": "Datum der letzten Aktualisierung in der Form jjjj-mm-tt.",
						"type": "lastedit"
					},
					"show": {
						"label": "Anzeigemodus",
						"description": "Legt fest, ob POIs und Koordinaten angezeigt werden sollen.",
						"type": "enum",
						"enum": [
							{
								"data": "all",
								"label": "POI und Koordinaten"
							},
							{
								"data": "poi",
								"label": "POI (Standard)"
							},
							{
								"data": "coordinates",
								"label": "Koordinaten"
							},
							{
								"data": "none",
								"label": "keine Koordinate"
							},
							{
								"data": "subtype",
								"label": "Merkmale anzeigen"
							},
							{
								"data": "nosubtye",
								"label": "Merkmale nicht anzeigen"
							},
							{
								"data": "inline",
								"label": "hängender Einzug"
							},
							{
								"data": "indent",
								"label": "innerhalb der Zeile"
							}
						],
						"hidden": true,
						"multiple": true
					},
					"group": {
						"label": "POI-Gruppe",
						"description": "Wert überschreibt die automatische ermittelte Gruppenzugehörigkeit z. B. mit buy, do, drink, eat, go, see oder sleep",
						"type": "enum",
						"enum": "group",
						"hidden": true
					},
					"map-group": {
						"label": "Kartengruppe",
						"description": "Die Angabe wird nur gebraucht, wenn Marker auf unterschiedliche Karten verteilt werden sollen.",
						"type": "string",
						"hidden": true
					}
				}
			}
		]
	}
}
