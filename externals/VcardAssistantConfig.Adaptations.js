/**
 * @class
 * @constructor
 */
mw.VcardAssistantConfig.Adaptations = class {

	/**
	 * @typedef {{ groupNumber: number, label: string }} SubtypeObject
	 */

	/**
	 * @param {string} key
	 * @return {Promise<Object<string, Object>>}
	 */
	async queryEnum( key ) {
		switch ( key ) {
			case 'group': {
				const groups = await this.getGroups();

				return Object.keys( groups ).map( ( groupName ) => {
					const group = groups[ groupName ];
					const data = {
						color: group.color,
						data: groupName,
						label: group.label || group.group
					};
					return data;
				} );
			}
			case 'subtype': {
				/**
				 * @type {{groupMeanings: Object.<string, string>, subtypes: Object.<string, SubtypeObject>}} subtypeDeclaration
				 */
				const { groupMeanings, subtypes } = await this.getSubtypes();
				const sortedSubtypeNames = Object.keys( subtypes ).sort( ( a, b ) => {
					const left = subtypes[ a ];
					const right = subtypes[ b ];
					return left.groupNumber - right.groupNumber !== 0 ? left.groupNumber - right.groupNumber : left.label.localeCompare( right.label );
				} );

				const outList = [];
				let lastGroup = -1;
				for ( const subtypeName of sortedSubtypeNames ) {
					const subtype = subtypes[ subtypeName ];
					if ( lastGroup !== subtype.groupNumber ) {
						outList.push( {
							optgroup: groupMeanings[ subtype.groupNumber ] || subtype.groupNumber
						} );
						lastGroup = subtype.groupNumber;
					}
					const data = {
						data: subtypeName,
						label: ( subtype.label !== null ) ? formatSubtype( subtype.label ) : subtypeName
					};
					outList.push( data );
				}

				return outList;
			}
			case 'type': {
				const [ types, groups ] = await Promise.all( [
					this.getTypes(),
					this.getGroups()
				] );

				return Object.keys( types ).sort( ( a, b ) => {
					return types[ a ].label.localeCompare( types[ b ].label );
				} ).map( ( typeName ) => {
					const type = types[ typeName ];
					const data = {
						color: groups[ type.group ].color ?? null,
						data: typeName,
						label: type.label ?? typeName
					};

					return data;
				} );
			}
			default:
				throw new Error( 'Illegal key' );
		}

		/**
		 * @param {string} subtypeName
		 * @return {string} Formatted subtype
		 */
		function formatSubtype( subtypeName ) {
			return subtypeName.replace( /\[(?:[^[\]]*)\|([^[\]]*)\]/ig, '$1' ).replace( /\[([^[\]]*)\]/ig, '' );
		}
	}

	/**
	 * @param {number} sectionIndex
	 * @param {number} vcardIndex
	 * @param {Function} clickCallback
	 * @return {HTMLElement}
	 */
	getEditButton( sectionIndex, vcardIndex, clickCallback ) {
		let $editButton = $( `<a href="javascript:void('editvcard-${sectionIndex}-${vcardIndex}')" title="Diese vCard bearbeiten">Bearbeiten</a>` )
			.on( 'click', clickCallback );
		$editButton = $( '<span>' ).append( $editButton );
		$editButton = $( '<span>' ).addClass( 'listing-metadata-item' )
			.addClass( 'listing-edit-button' )
			.addClass( 'noprint' )
			.append( $editButton );
		return $editButton[ 0 ];
	}

	/**
	 * @private
	 * @return {Promise}
	 */
	async getGroups() {
		const e = await $.ajax( {
			data: {
				title: 'Modul:Marker utilities/Groups',
				action: 'raw',
				ctype: 'text/plain'
			},
			method: 'GET',
			url: mw.util.wikiScript( '' )
		} );
		// FIXME: Use same approach as subtype/type
		const lineRegexp = /[ \t]+(([a-zA-Z0-9 _]+?)|'([a-zA-Z0-9 _]+)'|"([a-zA-Z0-9 _]+)"|\['([a-zA-Z0-9 _]+)'\]|\["([a-zA-Z0-9 _]+)"\]) *= *{(.+?)}/g;
		let match;
		const jsonLines = [];

		do {
			match = lineRegexp.exec( e );
			if ( match ) {
				const parameterName = match[ 2 ] || match[ 3 ] || match[ 4 ] || match[ 5 ] || match[ 6 ];
				let definition = match[ 7 ];
				definition = definition.replace( /(^|,) ([^ ]+) = /g, '$1"$2":' );
				jsonLines.push( `"${parameterName.replace( /_/g, ' ' )}": {${definition}}` );
			}
		} while ( match );

		return JSON.parse( `{${jsonLines.join( ',' )}}` );
	}

	/**
	 * @param {Object} prefill
	 * @return {number}
	 */
	getOutdatedThreshold( prefill ) {
		return 2 * 365 * 24 * 60;
	}

	/**
	 * @private
	 * @return {Promise<{groupMeanings: Object.<string, string>, subtypes: Object.<string, SubtypeObject>}>}
	 */
	async getSubtypes() {
		const e = await $.ajax( {
			data: {
				title: 'Modul:VCard/Subtypes',
				action: 'raw',
				ctype: 'text/plain'
			},
			method: 'GET',
			url: mw.util.wikiScript( '' )
		} );

		// Subtypes
		const lines = e.split( '\n' );
		const declarationRegex = /(?:\["(?<nameBrackted>[^"]+)"\]|(?<nameNonBracketed>[\w]+)) *= *\{(?<body>[^\n]+)\}/; // Extracts subtypetype name and "body" of declaration. We're assuming that there's at most one declaration per line and that each declaration takes up exactly one line
		const groupRegex = /g[\s\t]*=[\s\t]*(?<group>\d+)/; // Extracts group
		const labelRegex = /n[\s\t]*=[\s\t]*"(?<label>[^"]+)"/; // Extracts label

		/**
		 * @type {Object.<string, SubtypeObject>}
		 */
		const subtypes = {};
		for ( const line of lines ) {
			const declarationEval = declarationRegex.exec( line );
			if ( declarationEval ) {
				const name = declarationEval.groups.nameBracketed ?? declarationEval.groups.nameNonBracketed;
				const body = declarationEval.groups.body;

				// Group
				const groupEval = groupRegex.exec( body );
				const group = groupEval ? groupEval.groups.group : null;

				// Label
				const labelEval = labelRegex.exec( body );
				const label = labelEval ? labelEval.groups.label : null;

				subtypes[ name ] = {
					groupNumber: parseInt( group ),
					label: label
				};
			}
		}

		// Subtype group meanings
		const meaningBodyRegex = /[\s\S]*[\t ]g = {(?<body>[^}]+)}/gm;
		const meaningDeclarationRegex = /(?:\["(?<nameBracketed>[^"]+)"\]|(?<nameNonBracketed>[\w]+))[\s\t]*=[\s\t]*"(?<label>[^"]+)"/;

		/**
		 * @type {Object.<string, string>}
		 */
		const groupNumberMeanings = {};
		const meaningBodyEval = meaningBodyRegex.exec( e );
		if ( meaningBodyEval ) {
			for ( const line of meaningBodyEval.groups.body.split( '\n' ) ) {
				const meaningDeclarationEval = meaningDeclarationRegex.exec( line );
				if ( meaningDeclarationEval ) {
					groupNumberMeanings[ meaningDeclarationEval.groups.nameBracketed ?? meaningDeclarationEval.groups.nameNonBracketed ] = meaningDeclarationEval.groups.label;
				}
			}
		}

		return {
			groupMeanings: groupNumberMeanings,
			subtypes: subtypes
		};
	}

	/**
	 * @private
	 * @return {Promise<Object.<string, { group: string, label: string }>>}
	 */
	async getTypes() {
		/** @type {string} e */
		const e = await $.ajax( {
			data: {
				title: 'Modul:Marker utilities/Types',
				action: 'raw',
				ctype: 'text/plain'
			},
			method: 'GET',
			url: mw.util.wikiScript( '' )
		} );

		const lines = e.split( '\n' );
		const declarationRegex = /(?:\["(?<nameBracketed>[^"]+)"\]|(?<nameNonBracketed>[\w]+))[\s\t]*=[\s\t]*\{(?<body>[^\n]+)\}/; // Extracts type name and "body" of declaration. We're assuming that there's at most one declaration per line and that each declaration takes up exactly one line
		const groupRegex = /group[\s\t]*=[\s\t]*"(?<group>[^"]+)"/; // Extracts group, we're using several regexes to prevent requiring a specific order
		const labelRegex = /label[\s\t]*=[\s\t]*"(?<label>[^"]+)"/; // Extracts label

		/** @type {Object.<string, { group: string, label: string }>} */
		const groups = {};

		for ( const line of lines ) {
			const declarationEval = declarationRegex.exec( line );
			if ( declarationEval ) {
				const name = declarationEval.groups.nameBracketed ?? declarationEval.groups.nameNonBracketed;
				const body = declarationEval.groups.body;

				// Group
				const groupEval = groupRegex.exec( body );
				const group = groupEval ? groupEval.groups.group : null;

				const labelEval = labelRegex.exec( body );
				const label = labelEval ? labelEval.groups.label : null;

				if ( group !== null && label !== null ) {
					groups[ name ] = {
						group: group,
						label: label
					};
				}
			}
		}

		return groups;
	}

	/**
	 * @param {string} parameterName
	 * @param {string} timestamp
	 * @return {Date}
	 */
	parseTimestamp( parameterName, timestamp ) {
		return new Date( timestamp );
	}
};
