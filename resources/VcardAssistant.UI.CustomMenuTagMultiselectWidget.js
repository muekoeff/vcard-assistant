/**
 * @class
 * @constructor
 * @extends OO.ui.MenuTagMultiselectWidget
 * @param {Object.<string,string>} colorMap
 * @param {Object} [config]
 * @property {boolean} allowSearchForData
 * @property {Object.<string,string>} colorMap
 * @property {number} pendingCounter
 */
mw.VcardAssistant.UI.CustomMenuTagMultiselectWidget = function ( colorMap, config ) {
	config.menu = config.menu ?? {};
	config.menu.filterMode = config.menu.filterMode ?? 'substring';
	mw.VcardAssistant.UI.CustomMenuTagMultiselectWidget.super.call( this, config );

	this.allowSearchForData = config.allowSearchForData ?? true;
	this.colorMap = colorMap ?? null;
	this.pendingCounter = 0;
};
OO.inheritClass( mw.VcardAssistant.UI.CustomMenuTagMultiselectWidget, OO.ui.MenuTagMultiselectWidget );

/**
 * Add options to the menu
 *
 * @param {Object[]} menuOptions Object defining options
 * @return {void}
 */
mw.VcardAssistant.UI.CustomMenuTagMultiselectWidget.prototype.addOptions = function ( menuOptions ) {
	const items = menuOptions.map( ( obj ) => {
		return this.createMenuOptionWidget( obj );
	} );

	this.menu.addItems( items );
};

/**
 * Create a menu option widget.
 *
 * @param {{data: string, label: string, icon: string, optgroup: string}} obj Item data
 * @return {OO.ui.OptionWidget} Option widget
 */
mw.VcardAssistant.UI.CustomMenuTagMultiselectWidget.prototype.createMenuOptionWidget = function ( obj ) {
	if ( obj.optgroup !== undefined ) {
		return new OO.ui.MenuSectionOptionWidget( {
			label: ''.concat( obj.optgroup )
		} );
	} else {
		const menuOptionWidget = new OO.ui.MenuOptionWidget( {
			data: obj.data,
			label: obj.label ?? obj.data,
			icon: obj.icon
		} );

		const color = this.getColorFormDataItem( obj.data );
		if ( color !== null ) {
			menuOptionWidget.$label.prepend( `<span class="indicatordot" style="background-color:${color}"></span>` );
		}
		menuOptionWidget.$element.attr( 'title', obj.data );
		if ( this.allowSearchForData ) {
			menuOptionWidget.getMatchText = function () {
				return `${obj.label}, ${obj.data}`;
			};
		}
		return menuOptionWidget;
	}
};

/**
 * @param {string} data
 * @param {string} label
 * @return {OO.ui.TagItemWidget}
 */
mw.VcardAssistant.UI.CustomMenuTagMultiselectWidget.prototype.createTagItemWidget = function ( data, label ) {
	label = label || data;

	const tagItemWidget = new OO.ui.TagItemWidget( { data: data, label: label } );
	const color = this.getColorFormDataItem( data );
	if ( color !== null ) {
		tagItemWidget.$label.prepend( `<span class="indicatordot" style="background-color:${color}"></span>` );
	}
	tagItemWidget.$element.attr( 'title', data );
	return tagItemWidget;
};

/**
 * Returns the color of the given item or `null` if none defined.
 *
 * @param {string} dataItem
 * @return {string}
 */
mw.VcardAssistant.UI.CustomMenuTagMultiselectWidget.prototype.getColorFormDataItem = function ( dataItem ) {
	if ( this.colorMap !== null && this.colorMap[ dataItem ] !== undefined ) {
		return this.colorMap[ dataItem ];
	} else {
		return null;
	}
};

/**
 * Decrease pending counter. If it is 0 pending state will be removed from the element.
 *
 * @return {void}
 */
mw.VcardAssistant.UI.CustomMenuTagMultiselectWidget.prototype.popPending = function () {
	this.pendingCounter--;
	if ( this.pendingCounter === 0 ) {
		this.$element.find( 'input[type=text]' ).removeClass( 'oo-ui-pendingElement-pending' );
	}
};

/**
 * Increase pending counter. If it was 0 the element will be set to pending state.
 *
 * @return {void}
 */
mw.VcardAssistant.UI.CustomMenuTagMultiselectWidget.prototype.pushPending = function () {
	this.pendingCounter++;
	this.$element.find( 'input[type=text]' ).addClass( 'oo-ui-pendingElement-pending' );
};
