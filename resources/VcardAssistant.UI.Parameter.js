/* eslint-disable no-jquery/variable-pattern */
/**
 * @class
 * @constructor
 * @param {Object} vcardAssistantConfig
 * @param {mw.VcardAssistant.Adaptations} adaptations Resolver for wiki-specific data and logic
 * @param {string} parameterName
 * @param {Object} options
 * @param {string} prefill Initial value for this parameter
 * @param {mw.VcardAssistant.UI.EditForm} editForm
 * @property {string} description
 * @property {mw.VcardAssistant.UI.EditForm} editForm
 * @property {Object} enum
 * @property {string} example
 * @property {boolean} excluded
 * @property {Array<Object>} format
 * @property {boolean} hidden
 * @property {OO.ui.FieldLayoutWidget} inputFieldLayoutWidget
 * @property {OO.ui.InputWidget} inputWidget
 * @property {string} label
 * @property {number} max
 * @property {OO.ui.PopupWidget} messagePopupWidget
 * @property {number} min
 * @property {number} multiline
 * @property {boolean} multiple
 * @property {boolean} neverOmit
 * @property {mw.VcardAssistant.UI.ParameterButton} parameterButton
 * @property {string} parameterName
 * @property {string} prefill Initial value for this parameter
 * @property {Object} preview
 * @property {OO.ui.ButtonWidget} previewButton
 * @property {Object} triggers
 * @property {string} type
 * @property {Object} wikidata
 * @property {OO.ui.ButtonWidget} wikidataButton
 * @property {{claimId: string, claimValue: string, wikidataId: string}} wikidataClaim
 * @property {OO.ui.ToggleButtonWidget} updateLastEditButton Only set for type lastedit
 * @property {Function} getUpdateLastEdit Only set for type lastedit
 * @property {Function} setUpdateLastEdit Only set for type lastedit
 */
mw.VcardAssistant.UI.Parameter = function ( vcardAssistantConfig, adaptations, parameterName, options, prefill, editForm ) {
	this.vcardAssistantConfig = vcardAssistantConfig;
	this.adaptations = adaptations;
	this.prefill = prefill;
	this.editForm = editForm;

	this.excluded = false;
	this.inputFieldLayoutWidget = null;
	this.inputWidget = null;
	this.messagePopupWidget = null;
	this.parameterButton = null;
	this.previewButton = null;
	this.wikidataButton = null;
	this.wikidataClaim = null;

	this.description = options.description ?? null;
	this.enum = options.enum ?? null;
	this.example = options.example ?? null;
	this.format = options.format ?? null;
	this.hidden = options.hidden ?? false;
	this.label = options.label;
	this.max = options.max ?? null;
	this.min = options.min ?? null;
	this.multiline = options.multiline ?? null;
	this.multiple = options.multiple ?? null;
	this.neverOmit = options.neverOmit ?? false;
	this.parameterName = parameterName;
	this.preview = options.preview ?? null;
	this.required = options.required ?? false;
	this.triggers = options.triggers ?? null;
	this.type = options.type;
	if ( options.wikidata === undefined ) {
		this.wikidata = null;
	} else if ( typeof options.wikidata === 'string' ) {
		this.wikidata = {
			property: options.wikidata
		};
	} else {
		this.wikidata = options.wikidata;
	}

	// Setup EventTarget
	const eventDelegate = document.createDocumentFragment();
	this.on = function ( ...args ) {
		eventDelegate.addEventListener.apply( eventDelegate, args );
	};
	this.emit = function ( ...args ) {
		return eventDelegate.dispatchEvent.apply( eventDelegate, args );
	};
	this.off = function ( ...args ) {
		eventDelegate.removeEventListener.apply( eventDelegate, args );
	};

	// Implications
	switch ( this.type ) {
		case 'commons-category':
		case 'commons-file':
			this.format = this.format || [];
			this.format.push( {
				message: mw.msg( 'va-msg-format-commons-mayNotIncludePrefixes' ),
				regexp: '^[^:]*$'
			} );

			if ( this.type === 'commons-category' ) {
				this.preview = this.preview || [];
				this.preview.push( {
					type: 'url',
					url: 'https://commons.wikimedia.org/wiki/Category:{parameter}'
				} );
			}
			break;
		case 'lastedit':
			this.format = this.format || [];
			// regexp from https://stackoverflow.com/a/3809435
			this.format.push( {
				message: mw.msg( 'va-msg-format-date' ),
				regexp: '^20[0-9]{2}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])$'
			} );
			break;
		case 'url':
			this.format = this.format || [];
			// regexp from https://stackoverflow.com/a/3809435
			this.format.push( {
				regexp: 'http(s)?:\\/\\/[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)'
			} );

			this.preview = this.preview || [];
			this.preview.push( {
				type: 'url',
				url: '{parameter}'
			} );
			break;
		case 'wikidata-item-id':
			this.format = this.format || [];
			this.format.push( {
				message: mw.msg( 'va-msg-format-wikidataItemId-qxxx' ),
				regexp: '^Q[0-9]+$'
			} );

			this.preview = this.preview || [];
			this.preview.push( {
				type: 'url',
				url: 'https://www.wikidata.org/wiki/{parameter}'
			} );
			break;
	}
};

/**
 * @event mw.VcardAssistant.UI.Parameter#blur
 */

/**
 * @event mw.VcardAssistant.UI.Parameter#change
 */

/**
 * @event mw.VcardAssistant.UI.Parameter#focus
 */

/**
 * @event mw.VcardAssistant.UI.Parameter#validate
 * @property {boolean} valid
 * @property {string} value
 */

/**
 * @event mw.VcardAssistant.UI.Parameter#wikidataidchange
 * @property {string} wikidataid
 */

/**
 * @method
 * @extends EventTarget#dispatchEvent
 * @return {boolean}
 */
mw.VcardAssistant.UI.Parameter.prototype.emit = null; // Set in constructor

/**
 * @method
 * @extends EventTarget#removeEventListener
 */
mw.VcardAssistant.UI.Parameter.prototype.off = null; // Set in constructor

/**
 * @method
 * @extends EventTarget#addEventListener
 */
mw.VcardAssistant.UI.Parameter.prototype.on = null; // Set in constructor

/**
 * Applies the given claim to this parameter.
 *
 * @param {string} claimValue
 * @param {string} claimId
 * @param {string} wikidataId
 * @return {void}
 */
mw.VcardAssistant.UI.Parameter.prototype.applyWikidata = function ( claimValue, claimId, wikidataId ) {
	this.inputFieldLayoutWidget.$element.addClass( 'has-wikidata' );
	if ( this.wikidataButton !== null ) {
		this.wikidataButton.setDisabled( false );
		this.wikidataButton.setTitle( mw.msg( 'va-msg-wikidata-editOnWikidata' ) );
		this.wikidataButton.$button.attr( 'href', `https://www.wikidata.org/wiki/${wikidataId}#${claimId}` );
	}
	this.wikidataClaim = {
		claimId: claimId,
		claimValue: claimValue,
		wikidataId: wikidataId
	};
	this.inputWidget.$input.attr( 'placeholder', `Wikidata: ${claimValue}` );

	const event = new Event( 'wikidataidchange' );
	event.wikidataId = wikidataId;
	this.emit( event );
};

/**
 * Removed any information related to wikidata claims from this parameter.
 *
 * @return {void}
 */
mw.VcardAssistant.UI.Parameter.prototype.clearWikidata = function () {
	this.inputFieldLayoutWidget.$element.removeClass( 'has-wikidata' );
	if ( typeof this.wikidataButton !== 'undefined' ) {
		this.wikidataButton.setDisabled( true );
		this.wikidataButton.setTitle( mw.msg( 'va-msg-wikidata-canBeObtainedFromWikidata' ) );
		this.wikidataButton.$button.removeAttr( 'href' );
		this.wikidataButton.$button.removeAttr( 'target' );
	}
	this.wikidataClaim = null;
	this.inputWidget.$input.attr( 'placeholder', this.example !== null ? mw.msg( 'va-msg-parameter-forExample', this.example ) : '' );
};

/**
 * Generates a ParameterButton if needed and returns it.
 *
 * @return {mw.VcardAssistant.UI.ParameterButton}
 */
mw.VcardAssistant.UI.Parameter.prototype.getParameterButton = function () {
	if ( this.parameterButton !== null ) {
		return this.parameterButton;
	} else {
		this.parameterButton = new mw.VcardAssistant.UI.ParameterButton( this.editForm, this, this.prefill !== null );
		return this.parameterButton;
	}
};

/**
 * Generates an input for this parameter (together with an containing field) if needed and returns the input.
 *
 * @return {OO.ui.InputWidget}
 */
mw.VcardAssistant.UI.Parameter.prototype.getParameterInputField = function () {
	/**
	 * @param {mw.VcardAssistant.UI.Parameter} parameter
	 * @param {boolean} value
	 * @return {void}
	 */
	function toggleUpdateLastEdit( parameter, value ) {
		parameter.updateLastEditButton.setValue( value );
	}

	if ( this.inputFieldLayoutWidget !== null ) {
		return this.inputFieldLayoutWidget;
	} else {
		const fieldLayoutConfig = {
				align: 'top',
				label: this.label
			},
			inputWidgetConfig = {};

		if ( this.description !== null ) {
			fieldLayoutConfig.help = this.description;
		}
		if ( this.example !== null ) {
			inputWidgetConfig.placeholder = mw.msg( 'va-msg-parameter-forExample', this.example );
		}
		if ( this.max !== null ) {
			inputWidgetConfig.max = this.max;
		}
		if ( this.min !== null ) {
			inputWidgetConfig.min = this.min;
		}
		if ( this.multiline !== null ) {
			inputWidgetConfig.rows = this.multiline;
		}
		if ( this.prefill !== null ) {
			fieldLayoutConfig.value = this.prefill;
		}
		if ( this.required !== null ) {
			inputWidgetConfig.required = this.required;
		}

		let actualInput = null;

		switch ( this.type ) {
			case 'commons-category':
				this.inputWidget = new mw.VcardAssistant.UI.CommonsSearchField( 'Category', '14', inputWidgetConfig );
				actualInput = this.inputWidget.$input;
				this.inputWidget.getParameterValue = () => {
					return this.inputWidget.getValue();
				};
				if ( this.prefill !== null ) {
					this.inputWidget.setValue( this.prefill );
				}
				this.inputFieldLayoutWidget = new OO.ui.FieldLayout( this.inputWidget, fieldLayoutConfig );
				break;
			case 'commons-file':
				this.inputWidget = new mw.VcardAssistant.UI.CommonsSearchField( 'File', '6', inputWidgetConfig );
				actualInput = this.inputWidget.$input;
				this.inputWidget.getParameterValue = () => {
					return this.inputWidget.getValue();
				};
				if ( this.prefill !== null ) {
					this.inputWidget.setValue( this.prefill );
				}
				this.inputFieldLayoutWidget = new OO.ui.FieldLayout( this.inputWidget, fieldLayoutConfig );
				break;
			case 'enum':
				inputWidgetConfig.allowArbitrary = true;
				if ( this.multiple ?? false ) {
					inputWidgetConfig.inputPosition = 'outline';

					this.inputWidget = new mw.VcardAssistant.UI.CustomMenuTagMultiselectWidget( null, inputWidgetConfig );
					this.inputWidget.pushPending();
					this.inputWidget.getParameterValue = () => {
						return this.inputWidget.getValue().map( ( item ) => item ).join( ', ' );
					};
					this.getEnumItems().finally( () => {
						this.inputWidget.popPending();
					} ).then( ( v ) => {
						const colorMap = {};
						for ( const key of Object.keys( v ) ) {
							colorMap[ v[ key ].data ] = v[ key ].color ?? null;
						}

						this.inputWidget.colorMap = colorMap;
						this.inputWidget.addOptions( v );

						// Re-add items in order to update labels
						if ( this.inputWidget.getParameterValue().length > 0 ) {
							this.inputWidget.setValue( this.inputWidget.getParameterValue().split( ',' ) );
						}
					} ).catch( ( e ) => {
						mw.log.error( e );
						// @TODO: Handle error
					} );
					if ( this.prefill !== null ) {
						this.inputWidget.setValue( this.prefill.replace( / ?, /g, ',' ).split( ',' ) );
					}
					this.inputFieldLayoutWidget = new OO.ui.FieldLayout( this.inputWidget, fieldLayoutConfig );
				} else {
					this.inputWidget = new OO.ui.DropdownInputWidget( inputWidgetConfig );
					if ( this.prefill !== null ) {
						// Temporarily add item for current value. List will be recreated once data has been loaded
						this.inputWidget.setOptions( [
							{
								data: '',
								label: mw.msg( 'va-msg-list-novalue' )
							},
							{
								data: this.prefill,
								label: this.prefill
							}
						] );
						this.inputWidget.setValue( this.prefill );
					}
					this.inputWidget.getParameterValue = () => {
						return this.inputWidget.getValue();
					};
					this.getEnumItems().then( ( v ) => {
						if ( this.prefill !== null && v.find( ( item ) => item.data === this.prefill ) === undefined ) {
							v.unshift( {
								data: this.prefill,
								label: mw.msg( 'va-msg-list-unknownvalue', this.prefill )
							} );
						}

						v.unshift( {
							data: '',
							label: mw.msg( 'va-msg-list-novalue' )
						} );
						this.inputWidget.setOptions( v );

						if ( this.prefill !== null ) {
							this.inputWidget.setValue( this.prefill );
						}
					} ).catch( ( e ) => {
						mw.log.error( e );
						// @TODO: Handle error
					} );

					this.inputFieldLayoutWidget = new OO.ui.FieldLayout( this.inputWidget, fieldLayoutConfig );
				}
				break;
			case 'lastedit':
				this.inputWidget = new OO.ui.TextInputWidget();
				actualInput = this.inputWidget.$input;
				this.inputWidget.getParameterValue = () => {
					return this.inputWidget.getValue();
				};
				if ( this.prefill !== null ) {
					this.inputWidget.setValue( this.prefill );
				}
				this.inputWidget.oldvalue = '';

				const updateButton = new OO.ui.ToggleButtonWidget( {
					label: mw.msg( 'va-msg-parameter-markAsUpdated' ),
					title: mw.msg( 'va-msg-parameter-markAsUpdated-description' )
				} );
				this.inputFieldLayoutWidget = new OO.ui.ActionFieldLayout( this.inputWidget, updateButton, fieldLayoutConfig );
				updateButton.on( 'change', ( value ) => {
					if ( value ) {
						const date = new Date();
						this.inputWidget.oldvalue = this.inputWidget.getValue();
						this.inputWidget.setValue( `${String( date.getFullYear() ).padStart( 4, '0' )}-${String( date.getMonth() + 1 ).padStart( 2, '0' )}-${String( date.getDate() ).padStart( 2, '0' )}` );
					} else {
						this.inputWidget.setValue( this.inputWidget.oldvalue );
					}
				} );
				this.updateLastEditButton = updateButton;
				this.getUpdateLastEdit = () => {
					return updateButton.getValue();
				};
				this.setUpdateLastEdit = ( value ) => {
					toggleUpdateLastEdit( this, value );
				};
				break;
			case 'number':
				this.inputWidget = new OO.ui.NumberInputWidget( inputWidgetConfig );
				actualInput = this.inputWidget.$input;
				this.inputWidget.getParameterValue = () => {
					return this.inputWidget.getValue();
				};
				if ( this.prefill !== null ) {
					this.inputWidget.setValue( this.prefill );
				}
				this.inputFieldLayoutWidget = new OO.ui.FieldLayout( this.inputWidget, fieldLayoutConfig );
				break;
			case 'toggle':
				this.inputWidget = new OO.ui.ToggleSwitchWidget( inputWidgetConfig );
				this.inputWidget.getParameterValue = () => {
					return this.inputWidget.getValue() ? this.vcardAssistantConfig.config.togglevalue_on : this.vcardAssistantConfig.config.togglevalue_off;
				};
				if ( this.prefill !== null && /^j|ja|n|no|y|yes$/.test( this.prefill ) ) { // @TODO: move to Adaptations
					this.inputWidget.setValue( !/^n|no$/.test( this.prefill ) );
				}
				this.inputFieldLayoutWidget = new OO.ui.FieldLayout( this.inputWidget, fieldLayoutConfig );
				break;
			case 'wikidata-item-id':
				this.inputWidget = new mw.VcardAssistant.UI.WikidataSearchField( this.editForm, inputWidgetConfig );
				actualInput = this.inputWidget.$input;
				this.inputWidget.getParameterValue = () => {
					return this.inputWidget.getValue();
				};
				if ( this.prefill !== null ) {
					this.inputWidget.setValue( this.prefill );
				}
				this.inputFieldLayoutWidget = new OO.ui.FieldLayout( this.inputWidget, fieldLayoutConfig );
				break;
			case 'string':
			default:
				if ( this.type === 'string' && this.multiline !== null ) {
					this.inputWidget = new OO.ui.MultilineTextInputWidget( inputWidgetConfig );
					actualInput = this.inputWidget.$input;
					this.inputWidget.$element.css( 'resize', 'vertical' );
					this.inputWidget.getParameterValue = () => {
						return this.inputWidget.getValue();
					};
					if ( this.prefill !== null ) {
						this.inputWidget.setValue( this.prefill );
					}
					this.inputFieldLayoutWidget = new OO.ui.FieldLayout( this.inputWidget, fieldLayoutConfig );
				} else {
					this.inputWidget = new OO.ui.TextInputWidget( inputWidgetConfig );
					actualInput = this.inputWidget.$input;
					this.inputWidget.getParameterValue = () => {
						return this.inputWidget.getValue();
					};
					if ( this.prefill !== null ) {
						this.inputWidget.setValue( this.prefill );
					}
					this.inputFieldLayoutWidget = new OO.ui.FieldLayout( this.inputWidget, fieldLayoutConfig );

					if ( this.type === 'url' ) {
						actualInput.attr( 'type', 'url' );
					}
				}
		}

		this.inputFieldLayoutWidget.$label.attr( 'title', this.parameterName );

		// Add header buttons
		this.addHeaderButtons();

		// Validate prefill
		if ( this.prefill !== null ) {
			this.validate( true );
		}

		// Setup event dispatcher
		this.inputWidget.on( 'change', ( value ) => {
			const event = new Event( 'change' );
			event.value = value;
			this.emit( event );
		} );
		if ( actualInput !== null ) {
			actualInput.on( 'blur', () => {
				const event = new Event( 'blur' );
				this.emit( event );
			} );
			actualInput.on( 'focus', () => {
				const event = new Event( 'focus' );
				this.emit( event );
			} );
		}

		// Event handler
		this.on( 'blur', () => {
			this.validate( true );
		} );
		this.on( 'change', () => {
			this.validate( false );
		} );

		return this.inputFieldLayoutWidget;
	}
};

/**
 * Handles interpreting an enum object and returns a formatted list.
 *
 * @return {Promise<Object|OO.ui.Widget>}
 */
mw.VcardAssistant.UI.Parameter.prototype.getEnumItems = async function () {
	if ( typeof this.enum === 'string' ) {
		return await this.adaptations.queryEnum( this.enum );
	} else if ( typeof this.enum === 'object' ) {
		return this.enum.map( ( item ) => {
			let data;
			if ( typeof item === 'string' ) {
				data = {
					data: item,
					label: item
				};
				return data;
			} else {
				return {
					data: item.data,
					label: `${item.label ?? item.data}`
				};
			}
		} );
	}
};

/**
 * @return {OO.ui.PopupWidget}
 */
mw.VcardAssistant.UI.Parameter.prototype.getMessagePopupWidget = function () {
	if ( this.messagePopupWidget === null ) {
		this.messagePopupWidget = new OO.ui.PopupWidget( {
			$content: $( '<ul>' ),
			padded: true,
			width: 300
		} );
		this.messagePopupWidget.$element.on( 'click', ( e ) => {
			// If somebody wants to select and copy they can do so by pressing Ctrl
			if ( !e.ctrlKey ) {
				this.messagePopupWidget.toggle( false );
			}
		} );
		this.inputFieldLayoutWidget.$element.append( this.messagePopupWidget.$element );
	}
	return this.messagePopupWidget;
};

/**
 * @return {string} Value of the input.
 */
mw.VcardAssistant.UI.Parameter.prototype.getValue = function () {
	return this.inputWidget.getParameterValue();
};

/**
 * @param {Object} value Sets the value of the input.
 * @return {void}
 */
mw.VcardAssistant.UI.Parameter.prototype.setValue = function ( value ) {
	this.inputWidget.setValue( value );
};

/**
 * Toggles the visibility of the field.
 *
 * @param {boolean} target Whether to show or hide the field.
 * @return {void}
 */
mw.VcardAssistant.UI.Parameter.prototype.toggle = function ( target ) {
	this.inputFieldLayoutWidget.toggle( target );
	this.hidden = !target;
};

/**
 * Checks whether user input satisfies parameter's regexes. If not, all relevant warnings will be displayed.
 *
 * @param {boolean} showWarning Whether warnings should be displayed
 * @param {boolean} [suppressEvent]
 * @return {boolean} Whether all regexes are satisfied
 */
mw.VcardAssistant.UI.Parameter.prototype.validate = function ( showWarning, suppressEvent ) {
	const value = this.getValue();
	let result;

	if ( !this.required && value === '' ) {
		this.clearValidation( true, true );
		result = true;
	} else if ( this.required && value === '' ) {
		this.setValidationViolation( [ mw.msg( 'va-msg-format-obligatoryParameter-description' ) ], showWarning );
		result = false;
	} else if ( this.format === null ) {
		this.clearValidation( true, true );
		result = true;
	} else {
		const errorMessages = [];
		const isValid = this.format.reduce( ( accumulator, currentValue ) => {
			const validationResult = ( new RegExp( currentValue.regexp ) ).test( value );
			if ( !validationResult && currentValue.message !== undefined ) {
				errorMessages.push( currentValue.message );
			}
			return accumulator && validationResult;
		}, true );

		if ( isValid ) {
			this.clearValidation( true, true );
		} else {
			this.setValidationViolation( errorMessages, showWarning );
		}

		result = isValid;
	}

	if ( suppressEvent === undefined || !suppressEvent ) {
		const event = new Event( 'validate' );
		event.valid = result;
		event.value = this.getValue();
		this.emit( event );
	}

	return result;
};

/**
 * @return {boolean} Whether this parameter has been changed.
 */
mw.VcardAssistant.UI.Parameter.prototype.wasChanged = function () {
	if ( this.hidden ) {
		return this.prefill !== null && this.prefill !== '';
	} else {
		if ( this.prefill === null ) {
			// @TODO: what about toggles?
			return this.getValue() !== '';
		} else {
			return this.getValue() !== this.prefill;
		}
	}
};

/**
 * @private
 * @return {void}
 */
mw.VcardAssistant.UI.Parameter.prototype.addHeaderButtons = function () {
	const $header = this.inputFieldLayoutWidget.$element.find( '.oo-ui-fieldLayout-header' );

	// Wikidata button
	this.addWikidataHeaderButton( $header );

	// Preview button
	this.addPreviewHeaderButton( $header );
};

/**
 * @private
 * @param {jQuery<HTMLElement>} $header
 * @return {void}
 */
mw.VcardAssistant.UI.Parameter.prototype.addPreviewHeaderButton = function ( $header ) {
	const that = this;

	function setUrlHref( value, caller, parameters ) {
		const urlDeclaration = caller.preview.url.find( ( url ) => {
			return url.conditions === undefined || mw.VcardAssistant.UI.Condition.evaluateRawConjunctiveConditions( url.conditions, caller, parameters );
		} );
		caller.previewButton.$button.attr( 'href', urlDeclaration.url.replace( /{parameter}/, value ).replace( /{encodedparameter}/, encodeURIComponent( value ) ) );
	}

	function updateFromCoordinateParameters( valid, lat, long, previewButton, mapPopup ) {
		mapPopup.toggle( false );
		if ( mapPopup.destroyMap !== undefined ) {
			try {
				mapPopup.destroyMap();
			} catch ( ex ) {}
		}

		if ( ( lat.wikidataClaim !== null && long.wikidataClaim !== null ) || ( valid && lat.getValue() !== '' && long.getValue() !== '' ) ) {
			previewButton.setDisabled( false );
		} else {
			previewButton.setDisabled( true );
		}
	}

	if ( this.preview !== null ) {
		switch ( this.preview.type ) {
			case 'coordinates': {
				const mapPopup = new OO.ui.PopupWidget( {
					$content: $( `<i>${mw.msg( 'va-msg-preview-prepare' )}</i>` ),
					classes: [ 'mappreviewcontainer' ],
					height: 300,
					width: 300
				} );
				mapPopup.$element.append( new OO.ui.ProgressBarWidget( {
					progress: false
				} ) );
				let lat;
				let long;

				if ( this.preview.lat === '$this' ) {
					lat = this;
					long = this.editForm.parameters[ this.preview.long ];
				} else {
					lat = this.editForm.parameters[ this.preview.lat ];
					long = this;
				}

				this.previewButton = new OO.ui.ButtonWidget( {
					classes: [ 'ext-vcardassistant-headeraction', 'ext-vcardassistant-previewaction' ],
					disabled: true,
					framed: false,
					icon: 'map',
					title: mw.msg( 'va-msg-preview-map-description' )
				} );
				this.previewButton.$button.attr( 'rel', 'noopener noreferrer' )
					.attr( 'target', '_blank' );
				this.previewButton.$element.append( mapPopup.$element );
				this.previewButton.on( 'click', () => {
					mapPopup.toggle();

					if ( mapPopup.isVisible() ) {
						this.setupMapPreviewPopup( lat, long, mapPopup );
					}
				} );
				if ( $header.find( '.oo-ui-fieldLayout-help' ).length > 0 ) {
					$header.find( '.oo-ui-fieldLayout-help' ).after( this.previewButton.$element );
				} else {
					$header.prepend( this.previewButton.$element );
				}

				mw.VcardAssistant.Util.on( [ lat, long ], [ 'validate', 'wikidataidchange' ], () => {
					updateFromCoordinateParameters( lat.validate( false, true ) && long.validate( false, true ), lat, long, that.previewButton, mapPopup );
				} );
				break;
			}
			case 'url': {
				// Expand shorthand
				if ( typeof this.preview.url === 'string' ) {
					this.preview.url = [
						{
							url: this.preview.url
						}
					];
				}

				this.previewButton = new OO.ui.ButtonWidget( {
					classes: [ 'ext-vcardassistant-headeraction', 'ext-vcardassistant-previewaction' ],
					disabled: true,
					framed: false,
					icon: 'linkExternal',
					title: mw.msg( 'va-msg-preview-url-description' )
				} );
				this.previewButton.$button.attr( 'rel', 'noopener noreferrer' );
				this.previewButton.$button.attr( 'target', '_blank' );
				if ( $header.find( '.oo-ui-fieldLayout-help' ).length > 0 ) {
					$header.find( '.oo-ui-fieldLayout-help' ).after( this.previewButton.$element );
				} else {
					$header.prepend( this.previewButton.$element );
				}

				mw.VcardAssistant.Util.on( this, [ 'validate', 'wikidataidchange' ], ( e ) => {
					if ( that.validate( false, true ) && that.getValue() !== '' ) {
						that.previewButton.setDisabled( false );
						setUrlHref( e.value, that, that.editForm.parameters );
					} else if ( that.wikidataClaim !== null ) {
						that.previewButton.setDisabled( false );
						setUrlHref( that.wikidataClaim.claimValue, that, that.editForm.parameters );
					} else {
						that.previewButton.setDisabled( true );
					}
				} );
				break;
			}
		}
	}
};

/**
 * @private
 * @param {jQuery<HTMLElement>} $header
 * @return {void}
 */
mw.VcardAssistant.UI.Parameter.prototype.addWikidataHeaderButton = function ( $header ) {
	if ( this.wikidata !== null ) {
		this.wikidataButton = new OO.ui.ButtonWidget( {
			classes: [ 'ext-vcardassistant-headeraction', 'ext-vcardassistant-wikidataaction' ],
			disabled: true,
			framed: false,
			icon: 'logoWikidata',
			title: mw.msg( 'va-msg-wikidata-canBeObtainedFromWikidata' )
		} );
		this.wikidataButton.$button.attr( 'rel', 'noopener noreferrer' );
		this.wikidataButton.$button.attr( 'target', '_blank' );
		if ( $header.find( '.oo-ui-fieldLayout-help' ).length > 0 ) {
			$header.find( '.oo-ui-fieldLayout-help' ).after( this.wikidataButton.$element );
		} else {
			$header.prepend( this.wikidataButton.$element );
		}
	}
};

/**
 * @private
 * @param {boolean} field Whether to reset the field
 * @param {boolean} popup Whether to hide the popup
 * @return {void}
 */
mw.VcardAssistant.UI.Parameter.prototype.clearValidation = function ( field = true, popup = true ) {
	if ( field ) {
		this.inputFieldLayoutWidget.$element.removeClass( 'invalid' );
	}

	if ( popup && this.messagePopupWidget !== null ) {
		this.messagePopupWidget.toggle( false );
	}
};

/**
 * @private
 * @param {Object} trigger
 * @return {void}
 */
mw.VcardAssistant.UI.Parameter.prototype.performTriggerAction = function ( trigger ) {
	if ( typeof trigger.action.value !== 'undefined' ) { // Value
		this.setValue( trigger.action.value );
	} else if ( typeof trigger.action.visible !== 'undefined' ) { // Visibility
		if ( trigger.action.visible ) {
			this.toggle( true );
			this.inputFieldLayoutWidget.$element.addClass( 'fadein' );
		} else {
			this.toggle( false );
			this.inputFieldLayoutWidget.$element.removeClass( 'fadein' );
		}
	} else if ( typeof trigger.action.excluded !== 'undefined' ) { // Exclusion
		this.excluded = trigger.action.excluded;
	}
};

/**
 * @private
 * @param {mw.VcardAssistant.UI.Parameter} lat
 * @param {mw.VcardAssistant.UI.Parameter} long
 * @param {OO.ui.PopupWidget} mapPopup
 * @return {void}
 */
mw.VcardAssistant.UI.Parameter.prototype.setupMapPreviewPopup = function ( lat, long, mapPopup ) {
	mw.loader.using( [ 'ext.kartographer.box' ], () => {
		const kartoBox = mw.loader.require( 'ext.kartographer.box' );
		const mapContainer = $( mapPopup.$body[ 0 ] ).empty().append( '<div>' ).children().css( {
			height: 300
		} );

		let coordinates = null;
		const markers = [];
		// Wikidata marker
		if ( lat.wikidataClaim !== null && long.wikidataClaim !== null ) {
			markers.push( {
				type: 'Feature',
				properties: {
					'marker-color': '#9a0000',
					'marker-size': 'medium',
					'marker-symbol': 'marker',
					title: mw.msg( 'va-msg-preview-map-markerFromWikidata' ),
					description: `(${lat.wikidataClaim.claimValue}, ${long.wikidataClaim.claimValue})`
				},
				geometry: {
					type: 'Point',
					coordinates: [
						long.wikidataClaim.claimValue,
						lat.wikidataClaim.claimValue
					]
				}
			} );
			coordinates = [ lat.wikidataClaim.claimValue, long.wikidataClaim.claimValue ];
		}

		// Parameters marker
		if ( lat.validate( false, true ) && lat.getValue() !== '' && long.validate( false, true ) && long.getValue() !== '' ) {
			markers.push( {
				type: 'Feature',
				properties: {
					'marker-color': '#3366cc',
					'marker-size': 'medium',
					'marker-symbol': 'marker',
					title: mw.msg( 'va-msg-preview-map-markerFromParameter' ),
					description: `(${lat.getValue()}, ${long.getValue()})`
				},
				geometry: {
					type: 'Point',
					coordinates: [
						parseFloat( long.getValue() ),
						parseFloat( lat.getValue() )
					]
				}
			} );
			coordinates = [ parseFloat( lat.getValue() ), parseFloat( long.getValue() ) ];
		}

		const map = kartoBox.map( {
			allowFullScreen: true,
			container: mapContainer[ 0 ],
			center: coordinates,
			data: markers,
			zoom: 14
		} );
		mapPopup.destroyMap = function () {
			map.remove();
		};
	} );
};

/**
 * @private
 * @param {Object} trigger
 * @param {Array<mw.VcardAssistant.UI.Parameter>} parameters
 * @return {boolean}
 */
mw.VcardAssistant.UI.Parameter.prototype.setupTrigger = function ( trigger, parameters ) {
	const that = this;

	/**
	 * @param {Array<mw.VcardAssistant.UI.Condition>} conditions
	 * @return {boolean}
	 */
	function checkConditions( conditions ) {
		if ( mw.VcardAssistant.UI.Condition.evaluateConjunctiveConditions( conditions ) ) {
			that.performTriggerAction( trigger );
			return true;
		} else {
			return false;
		}
	}

	const conditionList = mw.VcardAssistant.UI.Condition.parseRawConditionList( trigger.conditions, that, parameters );
	for ( const condition of conditionList ) {
		condition.watchSender( () => {
			checkConditions( conditionList );
		} );
	}

	return checkConditions( conditionList );
};

/**
 * @param {Array<string>} errorMessages
 * @param {boolean} openPopup
 * @return {void}
 */
mw.VcardAssistant.UI.Parameter.prototype.setValidationViolation = function ( errorMessages, openPopup ) {
	if ( errorMessages.length > 0 ) {
		const messagePopupWidget = this.getMessagePopupWidget();
		$( messagePopupWidget.$body[ 0 ] ).html( `<ul>${errorMessages.map( ( val ) => `<li>${val}</li>` ).join( '' )}</ul>` );
		if ( openPopup ) {
			messagePopupWidget.toggle( true );
		}
	}
	this.inputFieldLayoutWidget.$element.addClass( 'invalid' );
};
