/**
 * @class
 * @constructor
 * @param {Object.<string, string>} options
 * @param {mw.VcardAssistant.UI.Parameter} caller
 * @param {Object.<string, mw.VcardAssistant.UI.Parameter>} parameters
 */
mw.VcardAssistant.UI.Condition = function ( options, caller, parameters ) {
	this.parameters = parameters;
	this.sender = options.sender !== undefined ? options.sender : '$this';
	this.senderParameter = this.sender === '$this' ? caller : null;
	this.value = options.value !== undefined ? options.value : null;
};

/**
 * @static
 * @param {Array<Object>} rawConditionList
 * @param {mw.VcardAssistant.UI.Parameter} caller
 * @param {Object.<string, mw.VcardAssistant.UI.Parameter>} parameters
 * @return {Array<mw.VcardAssistant.UI.Condition>}
 */
mw.VcardAssistant.UI.Condition.parseRawConditionList = function ( rawConditionList, caller, parameters ) {
	const conditionList = [];

	for ( const rawCondition of rawConditionList ) {
		const condition = new mw.VcardAssistant.UI.Condition( rawCondition, caller, parameters );
		conditionList.push( condition );
	}

	return conditionList;
};

/**
 * @return {boolean} Whether this condition is fullfilled or not.
 */
mw.VcardAssistant.UI.Condition.prototype.evaluate = function () {
	return this.getEvaluationFunction()();
};

/**
 * @return {Function} Function which returns whether this condition is fullfilled or not.
 */
mw.VcardAssistant.UI.Condition.prototype.getEvaluationFunction = function () {
	if ( this.sender !== null && this.value !== null ) {
		this.senderParameter = this.senderParameter || this.parameters[ this.sender ];

		return () => {
			return ( new RegExp( this.value ) ).test( this.senderParameter.getValue() );
		};
	}
};

/**
 * @param {Function} callback Function to call once the sender has changed.
 * @return {void}
 */
mw.VcardAssistant.UI.Condition.prototype.watchSender = function ( callback ) {
	this.senderParameter = this.senderParameter || this.parameters[ this.sender ];
	this.senderParameter.inputWidget.on( 'change', callback );
};

/**
 * @param {Array<mw.VcardAssistant.UI.Condition>} conditionList
 * @return {boolean} Whether all conditions in the given list evaluate to true.
 */
mw.VcardAssistant.UI.Condition.evaluateConjunctiveConditions = function ( conditionList ) {
	return conditionList.reduce( ( accumulator, currentCondition ) => accumulator && currentCondition.evaluate(), true );
};

/**
 * @param {Array<Object>} rawConditionList
 * @param {mw.VcardAssistant.UI.Parameter} caller
 * @param {Object.<string, mw.VcardAssistant.UI.Parameter>} parameters Object with all parameters.
 * @return {boolean} Whether all conditions in the given list evaluate to true.
 */
mw.VcardAssistant.UI.Condition.evaluateRawConjunctiveConditions = function ( rawConditionList, caller, parameters ) {
	return mw.VcardAssistant.UI.Condition.evaluateConjunctiveConditions( mw.VcardAssistant.UI.Condition.parseRawConditionList( rawConditionList, caller, parameters ) );
};
