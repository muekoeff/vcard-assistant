mw.VcardAssistant.Util = {};

/**
 * @static
 * @param {string} setting
 * @return {string}
 */
mw.VcardAssistant.Util.globalConfig = function ( setting ) {
	if ( window.vcardassistantconfig === undefined ) {
		return null;
	} else {
		return window.vcardassistantconfig[ setting ];
	}
};

/**
 * @static
 * @param {Object|Array<Object>} emitters
 * @param {Object|Array<Object>} events
 * @param {Function} handler
 * @return {void}
 */
mw.VcardAssistant.Util.on = function ( emitters, events, handler ) {
	if ( !Array.isArray( emitters ) ) {
		emitters = [ emitters ];
	}
	if ( !Array.isArray( events ) ) {
		events = [ events ];
	}

	for ( const emitter of emitters ) {
		for ( const event of events ) {
			emitter.on( event, handler );
		}
	}
};
