/* eslint-disable no-control-regex */
/**
 * @class
 * @constructor
 * @param {Object} vcardAssistantConfig
 * @param {mw.VcardAssistantConfig.Adaptations} adaptations
 */
mw.VcardAssistant = function ( vcardAssistantConfig, adaptations ) {
	this.vcardAssistantConfig = vcardAssistantConfig;
	this.adaptations = adaptations;

	mw.VcardAssistant.setupStrings( mw.VcardAssistantConfig.strings );
};

/**
 * @static
 * @param {Object} strings
 */
mw.VcardAssistant.setupStrings = function ( strings ) {
	const lang = mw.config.get( 'wgUserLanguage' );
	mw.messages.set( Object.fromEntries( Object.entries( strings ).map( ( [ stringKey, stringValue ] ) => {
		return [ `va-msg-${stringKey}`, stringValue[ lang ] ?? stringValue.en ];
	} ) ) );
};

/**
 * @param {number} sectionIndex
 * @param {number} vcardIndex
 * @param {boolean} displayBlock
 * @return {Promise}
 */
mw.VcardAssistant.prototype.editVcard = async function ( sectionIndex, vcardIndex, displayBlock ) {
	const that = this;

	async function performSave( dialog, sectionText, editSectionText, summary, autoSummary, isMinor ) {
		let finalSummary;
		const sectionName = that.getSectionName( sectionText );
		const sectionPrefix = sectionName === '' ? '' : `/* ${sectionName} */`;
		if ( summary !== '' ) {
			finalSummary = `${sectionPrefix}${mw.msg( 'va-msg-summary-format', summary, autoSummary )}`;
		} else {
			finalSummary = `${sectionPrefix}${autoSummary}`;
		}

		if ( editSectionText ) {
			// TODO: Edit section text
		}

		const notifySaving = mw.notification.notify( mw.msg( 'va-msg-notify-saving' ), {
			tag: 'vcardassistant',
			title: 'vCard-Assistant'
		} );

		try {
			await that.saveSection( sectionText, sectionIndex, sectionName, finalSummary, isMinor );
		} catch ( ex ) {
			mw.notify( mw.msg( 'va-msg-notify-errorSubmit' ), {
				tag: 'vcardassistant',
				title: 'vCard-Assistant',
				type: 'error'
			} );

			await OO.ui.alert( ex );

			// Reopen editor in order to prevent data-loss
			dialog.open();
			dialog.switchForm( dialog.editForm );
		} finally {
			notifySaving.close();
		}
	}

	await mw.loader.using( 'mediawiki.notification' );
	const notifyStarting = mw.notification.notify( mw.msg( 'va-msg-notify-preparing' ), {
		tag: 'vcardassistant',
		title: 'vCard-Assistant'
	} );

	try {
		const data = await $.ajax( {
			cache: false,
			data: {
				action: 'raw',
				section: sectionIndex,
				title: mw.config.get( 'wgPageName' )
			},
			timeout: 3000,
			url: mw.util.wikiScript( '' )
		} );

		const [ sectionText, replacements ] = this.stripComments( data );
		const vcardData = this.getVcardInSection( sectionText, vcardIndex );
		const vcardParameters = this.wikitextToParameterMap( vcardData.wikitext, displayBlock, replacements );

		notifyStarting.close();

		const [ dialog, result ] = await mw.VcardAssistant.UI.spawn( mw.VcardAssistant.UI.Mode.EDIT, this.vcardAssistantConfig, this.adaptations, vcardParameters );

		let pre = sectionText.slice( 0, vcardData.start );
		const post = sectionText.slice( vcardData.end );

		if ( result.action === mw.VcardAssistant.UI.Result.Action.EDIT ) {
			await performSave( dialog, this.restoreComments( `${pre}${result.wikitext}${post}`, replacements ), result.editSectionWikitext, result.summary, mw.msg( 'va-msg-summary-auto-edited', result.displayName ), result.isMinorEdit );
			// TODO: Autosummary
		} else if ( result.action === mw.VcardAssistant.UI.Result.Action.DELETE ) {
			pre = this.removeVcardResidues( pre );
			await performSave( dialog, this.restoreComments( `${pre}${post}`, replacements ), false, result.summary, mw.msg( 'va-msg-summary-auto-deleted', result.displayName ), false );
		} else {
			throw new Error( `Unexpected action returned by UI: ${result.action}` );
		}
	} catch ( ex ) {
		mw.notify( mw.msg( 'va-msg-notify-loadingFailed' ), {
			tag: 'vcardassistant',
			title: 'vCard-Assistant',
			type: 'error'
		} );
		throw ex;
	}
};

/**
 * Utility method for finding a matching end pattern for a specified start pattern, including nesting. The specified value must start with the start value, otherwise an empty string will be returned.
 *
 * @param {string} value
 * @param {string} startPattern
 * @param {string} endPattern
 * @return {string}
 */
mw.VcardAssistant.prototype.findPatternMatch = function ( value, startPattern, endPattern ) {
	let matchString = '';
	const startRegex = new RegExp( '^' + this.replaceSpecial( startPattern ), 'i' );
	if ( startRegex.test( value ) ) {
		const endRegex = new RegExp( '^' + this.replaceSpecial( endPattern ), 'i' );
		let matchCount = 1;
		for ( let i = startPattern.length; i < value.length; i++ ) {
			const remainingValue = value.slice( i );
			if ( startRegex.test( remainingValue ) ) {
				matchCount++;
			} else if ( endRegex.test( remainingValue ) ) {
				matchCount--;
			}
			if ( matchCount === 0 ) {
				matchString = value.slice( 0, i );
				break;
			}
		}
	}
	return matchString;
};

/**
 * @param {string} sectionText
 * @return {string}
 */
mw.VcardAssistant.prototype.getSectionName = function ( sectionText ) {
	const headingRegexp = /^=+\s*([^=]+)\s*=+\s*\n/;
	const result = headingRegexp.exec( sectionText );
	return result !== null ? result[ 1 ].trim() : '';
};

/**
 * Return a regular expression that can be used to find all listing template invocations (as configured via the LISTING_TEMPLATES map) within a section of wikitext. Note that the returned regex simply matches the start of the template ("{{listing") and not the full template ("{{listing|key=value|...}}").
 *
 * @return {RegExp}
 */
mw.VcardAssistant.prototype.getVcardTypesRegexp = function () {
	return new RegExp( '({{\\s*(' + Object.keys( this.vcardAssistantConfig.config.templateToType ).join( '|' ) + ')\\b)(\\s*[\\|}])', 'ig' );
};

/**
 * Given a listing index, return the full wikitext for that listing ("{{listing|key=value|...}}").
 *
 * @param {string} sectionText
 * @param {number} vcardIndex
 * @return {{end: number, start: number, wikitext: string}}
 */
mw.VcardAssistant.prototype.getVcardInSection = function ( sectionText, vcardIndex ) {
	// find the listing wikitext that matches the same index as the listing index
	const listingRegex = this.getVcardTypesRegexp();
	// look through all matches for "{{listing|see|do...}}" within the section wikitext, returning the nth match, where 'n' is equal to the index of the edit link that was clicked
	let listingSyntax, regexResult, listingMatchIndex;
	for ( let i = 0; i <= vcardIndex; i++ ) {
		regexResult = listingRegex.exec( sectionText );
		listingMatchIndex = regexResult.index;
		listingSyntax = regexResult[ 1 ];
	}
	// listings may contain nested templates, so step through all section text after the matched text to find MATCHING closing braces the first two braces are matched by the listing regex and already captured in the listingSyntax variable
	let curlyBraceCount = 2;
	const endPos = sectionText.length;
	const startPos = listingMatchIndex + listingSyntax.length;
	let matchFound = false;
	let vcardEndPos = sectionText.length;
	for ( let j = startPos; j < endPos; j++ ) {
		if ( sectionText[ j ] === '{' ) {
			++curlyBraceCount;
		} else if ( sectionText[ j ] === '}' ) {
			--curlyBraceCount;
		}
		if ( curlyBraceCount === 0 && ( j + 1 ) < endPos ) {
			listingSyntax = sectionText.slice( listingMatchIndex, j + 1 );
			matchFound = true;
			vcardEndPos = j + 1;
			break;
		}
	}
	if ( !matchFound ) {
		listingSyntax = sectionText.slice( listingMatchIndex );
		vcardEndPos = sectionText.length;
	}
	return {
		end: vcardEndPos,
		start: listingMatchIndex,
		wikitext: listingSyntax.trim()
	};
};

/**
 * Split the raw template wikitext into an array of params. The pipe symbol delimits template params, but this method will also inspect the content to deal with nested templates or wikilinks that might contain pipe characters that should not be used as delimiters.
 *
 * @param {string} vcardWikiSyntax
 * @return {Array<string>}
 */
mw.VcardAssistant.prototype.listingTemplateToParamsArray = function ( vcardWikiSyntax ) {
	const results = [];
	let paramValue = '';
	let pos = 0;
	while ( pos < vcardWikiSyntax.length ) {
		const remainingString = vcardWikiSyntax.slice( pos );
		// check for a nested template or wikilink
		let patternMatch = this.findPatternMatch( remainingString, '{{', '}}' );
		if ( patternMatch.length === 0 ) {
			patternMatch = this.findPatternMatch( remainingString, '[[', ']]' );
		}
		if ( patternMatch.length ) {
			paramValue += patternMatch;
			pos += patternMatch.length;
		} else if ( vcardWikiSyntax.charAt( pos ) === '|' ) {
			// delimiter - push the previous param and move on to the next
			results.push( paramValue );
			paramValue = '';
			pos++;
		} else {
			// append the character to the param value being built
			paramValue += vcardWikiSyntax.charAt( pos );
			pos++;
		}
	}
	if ( paramValue.length ) {
		// append the last param value
		results.push( paramValue );
	}
	return results;
};

/**
 * Remove controls and illegal characters.
 *
 * @param {string} str
 * @param {boolean} isContent
 * @param {boolean} displayBlock
 * @return {string}
 */
mw.VcardAssistant.prototype.removeCtrls = function ( str, isContent, displayBlock ) {
	if ( displayBlock && isContent ) {
		str = str.replace( /[\x00-\x09\x0B\x0C\x0E\x0F\x7F]/g, ' ' );
	} else {
		str = str.replace( /(<\/?br[^%/>]*\/*>|<\/?p[^%/>]*\/*>)/g, ' ' );
		if ( !str.includes( '<span' ) ) {
			str = str.replace( /[\x00-\x0F\x7F]/g, ' ' );
		}
	}
	return str.trim().replace( / {2,}/g, ' ' );
};

/**
 * @param {string} str
 * @return {string}
 */
mw.VcardAssistant.prototype.removeVcardResidues = function ( str ) {
	return str.replace( /(\n+[:*#]*)?\s*$/, '' );
};

/**
 * @param {string} str
 * @return {string}
 */
mw.VcardAssistant.prototype.replaceSpecial = function ( str ) {
	return str.replace( /[.?*+^$[\]\\(){}|-]/g, '\\$&' );
};

/**
 * Search the text provided, and if it contains any text that was previously stripped out for replacement purposes, restore it.
 *
 * @param {string} text
 * @param {Array<string>} replacements
 * @return {string}
 */
mw.VcardAssistant.prototype.restoreComments = function ( text, replacements ) {
	for ( const key in replacements ) {
		const val = replacements[ key ];
		text = text.replace( key, val );
	}
	return text;
};

/**
 * @param {string} sectionText
 * @param {number} sectionIndex
 * @param {string} sectionName
 * @param {string} summary
 * @param {boolean} isMinor
 * @return {Promise}
 */
mw.VcardAssistant.prototype.saveSection = function ( sectionText, sectionIndex, sectionName, summary, isMinor ) {
	const api = new mw.Api();
	return new Promise( ( resolve, reject ) => {
		api.postWithToken( 'csrf', {
			action: 'edit',
			title: mw.config.get( 'wgPageName' ),
			section: sectionIndex,
			text: sectionText,
			summary: summary,
			minor: isMinor
			/* captchaid: cid,
			captchaword: answer, */
		} ).done( ( data ) => {
			if ( data && data.edit && data.edit.result === 'Success' ) {
				// since the listing editor can be used on diff pages, redirect
				// to the canonical URL if it is different from the current URL
				const canonicalUrl = $( 'link[rel="canonical"]' ).attr( 'href' );
				const currentUrlWithoutHash = window.location.href.replace( window.location.hash, '' );
				const sectionHash = sectionName !== '' ? `#${mw.util.escapeIdForLink( sectionName )}` : '';
				const url = new URL( window.location.href );

				if ( canonicalUrl && currentUrlWithoutHash !== canonicalUrl ) {
					window.location.href = `${canonicalUrl}?vcardassistant=saved${sectionHash}`;
				} else if ( url.searchParams.get( 'vcardassistant' ) === 'saved' ) {
					window.location.hash = sectionHash;
					window.location.reload();
				} else {
					// @ts-ignore
					window.location = `?vcardassistant=saved${sectionHash}`;
				}
				resolve();
			} else if ( data && data.error ) {
				reject( mw.msg( 'va-msg-error-submitApi', data.error.code, data.error.info ) );
			} else if ( data && data.edit.spamblacklist ) {
				reject( mw.msg( 'va-msg-error-submitBlacklist', data.edit.spamblacklist ) );
			} else if ( data && data.edit.captcha ) {
				// @TODO: closeForm( SAVE_FORM_SELECTOR );
				// @TODO: captchaDialog( summary, minor, sectionNumber, data.edit.captcha.url, data.edit.captcha.id );
			} else {
				reject( mw.msg( 'va-msg-error-submitUnknown' ) );
			}
		} ).fail( ( code, result ) => {
			if ( code === 'http' ) {
				reject( mw.msg( 'va-msg-error-submitHttp', result.textStatus ) );
			} else if ( code === 'ok-but-empty' ) {
				reject( mw.msg( 'va-msg-error-submitEmpty' ) );
			} else {
				reject( mw.msg( 'va-msg-error-submitUnknown' ) );
			}
		} );
	} );
};

/**
 * Commented-out listings can result in the wrong listing being edited, so strip out any comments and replace them with placeholders that can be restored prior to saving changes.
 *
 * @param {string} text
 * @return {Array<string, Array<string>>}
 */
mw.VcardAssistant.prototype.stripComments = function ( text ) {
	const comments = text.match( /<!--[\s\S]*?-->/mig );
	/**
	 * @type {Array<string>}
	 */
	const replacements = [];

	if ( comments !== null ) {
		for ( let i = 0; i < comments.length; i++ ) {
			const comment = comments[ i ];
			const rep = '<<<COMMENT' + i + '>>>';
			text = text.replace( comment, rep );
			replacements[ rep ] = comment;
		}
	}

	return [ text, replacements ];
};

/**
 * Convert raw wiki listing syntax into a mapping of key-value pairs corresponding to the listing template parameters.
 *
 * @param {string} vcardWikiSyntax
 * @param {boolean} displayBlock
 * @param {Array<string>} replacements
 * @return {Object.<string, string>}
 */
mw.VcardAssistant.prototype.wikitextToParameterMap = function ( vcardWikiSyntax, displayBlock, replacements ) {
	// remove the trailing braces
	vcardWikiSyntax = vcardWikiSyntax.slice( 0, -2 );
	/**
	 * @type {Object.<string, string>}
	 */
	const vcardAsParameterMap = {};
	const listParams = this.listingTemplateToParamsArray( vcardWikiSyntax );
	let lastKey = null;
	let lastUnnamedParameterIndex = 0;
	for ( let j = 1; j < listParams.length; j++ ) {
		const param = listParams[ j ];
		const index = param.indexOf( '=' );
		if ( index > 0 ) {
			// param is of the form key=value
			const key = param.slice( 0, index ).trim();
			const value = param.slice( index + 1 ).trim();
			vcardAsParameterMap[ key ] = value;
			lastKey = key;
		} else if ( lastKey !== null && vcardAsParameterMap[ lastKey ].length > 0 ) {
			// there was a pipe character within a param value, such as "key=value1|value2", so just append to the previous param
			vcardAsParameterMap[ lastKey ] += '|' + param;
		} else {
			lastUnnamedParameterIndex++;

			// Make sure to skip already explicitly set indexes
			while ( Object.keys( vcardAsParameterMap ).includes( lastUnnamedParameterIndex.toString() ) ) {
				lastUnnamedParameterIndex++;
			}

			vcardAsParameterMap[ lastUnnamedParameterIndex ] = param;
		}
	}
	for ( const key in vcardAsParameterMap ) {
		// if the template value contains an HTML comment that was previously converted to a placehold then it needs to be converted back to a comment so that the placeholder is not displayed in the edit form
		vcardAsParameterMap[ key ] = this.restoreComments( vcardAsParameterMap[ key ], replacements );
	}

	for ( const categoryName in this.vcardAssistantConfig.parameters.categories ) {
		for ( const parameterName in this.vcardAssistantConfig.parameters.categories[ categoryName ] ) {
			if ( vcardAsParameterMap[ parameterName ] !== undefined ) {
				if ( typeof this.vcardAssistantConfig.parameters.categories[ categoryName ][ parameterName ].multiline === 'number' ) {
					vcardAsParameterMap[ parameterName ] = this.removeCtrls( vcardAsParameterMap[ parameterName ], true, displayBlock );

					// convert paragraph tags for all multiline parameters to newlines so that the content is more readable in the editor window
					vcardAsParameterMap[ parameterName ] = vcardAsParameterMap[ parameterName ].replace( /\s*<p>\s*/g, '\n\n' );
				} else {
					vcardAsParameterMap[ parameterName ] = this.removeCtrls( vcardAsParameterMap[ parameterName ], false, displayBlock );
				}
			}
		}
	}

	return vcardAsParameterMap;
};
