/**
 * Lightweight loader for vCard-Assistant.
 * Installs edit-links and, once clicked, loads vCard-Assistant.
 */
mw.loader.using( 'mediawiki.util', () => {
	let _vcardAssistant = null;

	/**
	 * Adds edit links to all vCards.
	 *
	 * @param {jQuery<HTMLElement>} $parent
	 * @param {Object} vcardAssistantConfig
	 * @param {mw.VcardAssistantConfig.Adaptations} adaptations
	 * @return {boolean}
	 */
	function addEditLinks( $parent, vcardAssistantConfig, adaptations ) {
		if ( $( vcardAssistantConfig.config.disableeditlinksifpresent.join( ',' ) ).length > 0 ) {
			return false;
		}

		let sectionIndex = 0;
		let vcardIndex = 0;
		$parent.find( `h1, h2, h3, h4, h5, h6, ${vcardAssistantConfig.config.editlinkcontainer}` ).each( function () { // `this` needed!
			if ( $( this ).is( 'h1, h2, h3, h4, h5, h6' ) ) {
				if ( $( this ).closest( '#toc' ).length === 0 ) {
					// Ignore TOC
					sectionIndex++;
					vcardIndex = 0;
				}
			} else {
				const localSectionIndex = sectionIndex;
				const localVcardIndex = vcardIndex;
				$( this ).append( adaptations.getEditButton( localSectionIndex, localVcardIndex, async () => {
					try {
						const vcardAssistant = await getVcardAssistant( vcardAssistantConfig, adaptations );
						vcardAssistant.editVcard( localSectionIndex, localVcardIndex, false );
					} catch ( ex ) {
						mw.notify( 'Fehler beim Laden des vCard-Assistenten', {
							tag: 'vcardassistant',
							title: 'vCard-Assistant',
							type: 'error'
						} );
						mw.log.error( '[VcardAssistant]', ex );
					}
				} ) );
				vcardIndex++;
			}
		} );
	}

	/**
	 * @param {Object} vcardAssistantConfig
	 * @param {mw.VcardAssistantConfig.Adaptations} adaptations
	 */
	function addPortlet( vcardAssistantConfig, adaptations ) {
		const portlet = mw.util.addPortletLink( 'p-tb', '#vcard-assistant', 'vCard-Assistant', 'p-vcardassistant', 'vCard-Assistenten öffnen' );
		$( portlet ).on( 'click', async ( portletE ) => {
			portletE.preventDefault();

			await getVcardAssistant( vcardAssistantConfig, adaptations );
			await mw.VcardAssistant.UI.spawn( mw.VcardAssistant.UI.Mode.TOOL, vcardAssistantConfig, adaptations );
		} );
	}

	/**
	 * @return {void}
	 */
	async function checkQuery() {
		const url = new URL( window.location.href );

		if ( url.searchParams.get( 'vcardassistant' ) === 'saved' ) {
			// Display "saved"-message.
			await mw.loader.using( 'mediawiki.action.view.postEdit' );
			mw.hook( 'postEdit' ).fire( {
				message: mw.msg( 'postedit-confirmation-saved', mw.user )
			} );
		}
	}

	/**
	 * @param {Object} vcardAssistantConfig
	 * @param {mw.VcardAssistantConfig.Adaptations} adaptations
	 * @return {Promise}
	 */
	async function getVcardAssistant( vcardAssistantConfig, adaptations ) {
		if ( _vcardAssistant !== null ) {
			return _vcardAssistant;
		}

		if ( mw.VcardAssistant === undefined ) {
			await mw.loader.using( 'mediawiki.notification' );
			const notification = mw.notification.notify( 'Assistent wird vorbereitet. Augenblick bitte…', {
				tag: 'vcardassistant',
				title: 'vCard-Assistant',
				type: 'info'
			} );

			await Promise.all( [
				mw.loader.using( [ 'oojs', 'oojs-ui', 'oojs-ui.styles.icons-accessibility', 'oojs-ui.styles.icons-editing-advanced', 'oojs-ui.styles.icons-editing-core', 'oojs-ui.styles.icons-location', 'oojs-ui.styles.icons-moderation' ] ),
				mw.loader.load( 'https://de.wikivoyage.org/w/index.php?title=User:Nw520/vCardAssistant.css&action=raw&ctype=text/css', 'text/css' )
			] );

			await mw.loader.getScript( 'https://de.wikivoyage.org/w/index.php?title=User:Nw520/vCardAssistant.js&action=raw&ctype=text/javascript' );
			notification.close();
		}

		_vcardAssistant = new mw.VcardAssistant( vcardAssistantConfig, adaptations );
		return _vcardAssistant;
	}

	function main( vcardAssistantConfig, adaptations ) {
		mw.hook( 'wikipage.content' ).add( ( $content ) => {
			addEditLinks( $content, vcardAssistantConfig, adaptations );
		} );
		addPortlet( vcardAssistantConfig, adaptations );
		checkQuery();
	}

	main( mw.VcardAssistantConfig.config, new mw.VcardAssistantConfig.Adaptations() );
} );
