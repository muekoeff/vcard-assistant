/**
 * @class
 * @constructor
 * @extends OO.ui.MenuOptionWidget
 * @param {Object} [config]
 */
mw.VcardAssistant.UI.SearchResult = function ( config ) {
	mw.VcardAssistant.UI.SearchResult.super.call( this, config );

	const $description = $( '<span>' )
		.addClass( 'ext-vcardassistant-description' )
		.append( $( '<bdi>' ).text( config.description || '' ) );
	this.$element.append( $description );
};
OO.inheritClass( mw.VcardAssistant.UI.SearchResult, OO.ui.MenuOptionWidget );
