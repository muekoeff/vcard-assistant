/**
 * @external OO
 * @see {@link https://doc.wikimedia.org/oojs-ui/master/js/|OO}
 */

/**
 * @class
 * @constructor
 * @extends OO.ui.ProcessDialog
 * @param {mw.VcardAssistant.Mode} mode
 * @param {OO.ui.WindowManager} windowManager
 * @param {Object} vcardAssistantConfig Configuration for vCard-Assistant
 * @param {Function} resolve
 * @param {Function} reject
 * @param {mw.VcardAssistantConfig.Adaptations} adaptations Resolver for wiki-specific data and logic
 * @param {Object.<string, string>} [prefill] Initial value for parameters
 * @param {Object} [config]
 * @property {mw.VcardAssistant.UI.EditForm} editForm
 * @property {mw.VcardAssistantConfig.Adaptations} adaptations Resolver for wiki-specific data and logic
 * @property {mw.VcardAssistantConfig.Adaptations} mode
 * @property {Object.<string, string>} prefill Initial value for parameters
 * @property {Object} vcardAssistantConfig Configuration for vCard-Assistant
 * @property {OO.ui.WindowManager} windowManager
 */
mw.VcardAssistant.UI = function ( mode, windowManager, vcardAssistantConfig, resolve, reject, adaptations, prefill, config ) {
	mw.VcardAssistant.UI.static.actions = [
		{
			action: 'close',
			flags: [ 'close', 'safe' ],
			icon: 'close',
			invisibleLabel: true,
			label: mw.msg( 'va-msg-action-discard' ),
			modes: [ 'edit', 'preview' ],
			title: mw.msg( 'va-msg-action-discard-description' )
		},
		{
			action: 'returntoedit',
			flags: [ 'back', 'safe' ],
			icon: 'previous',
			label: mw.msg( 'va-msg-action-returnToEdit' ),
			modes: 'preview',
			title: mw.msg( 'va-msg-action-returnToEdit-description' )
		},
		{
			action: 'help',
			framed: false,
			href: vcardAssistantConfig.config.helpurl,
			icon: 'info',
			label: mw.msg( 'va-msg-action-help' ),
			modes: [ 'edit', 'preview' ],
			title: mw.msg( 'va-msg-action-help-description' )
		},
		{
			action: 'preview',
			flags: [ 'primary', 'progressive' ],
			icon: 'eye',
			label: mw.msg( 'va-msg-action-preview' ),
			modes: 'edit',
			title: mw.msg( 'va-msg-action-preview-description' )
		}
	];
	if ( mode === mw.VcardAssistant.UI.Mode.EDIT ) {
		mw.VcardAssistant.UI.static.actions.push( {
			action: 'delete',
			flags: [ 'destructive' ],
			icon: 'trash',
			label: mw.msg( 'va-msg-action-delete' ),
			modes: 'edit',
			title: mw.msg( 'va-msg-action-delete-description' )
		} );
	}
	if ( mode === mw.VcardAssistant.UI.Mode.CREATE || mode === mw.VcardAssistant.UI.Mode.EDIT ) {
		mw.VcardAssistant.UI.static.actions.push( {
			action: 'save',
			flags: [ 'primary', 'progressive' ],
			icon: 'check',
			label: mw.msg( 'va-msg-action-publish' ),
			modes: 'preview',
			title: mw.msg( 'va-msg-action-publish-description' )
		} );
	} else if ( mode === mw.VcardAssistant.UI.Mode.TOOL ) {
		mw.VcardAssistant.UI.static.actions.push( {
			action: 'copytoclipboard',
			flags: [ 'primary', 'progressive' ],
			icon: 'code',
			label: mw.msg( 'va-msg-action-copywikitexttoclipboard' ),
			modes: 'preview',
			title: mw.msg( 'va-msg-action-copywikitexttoclipboard-description' )
		} );
	}
	mw.VcardAssistant.UI.static.title = vcardAssistantConfig.config.title;
	mw.VcardAssistant.UI.parent.call( this, config );

	this.adaptations = adaptations;
	this.mode = mode;
	this.prefill = prefill || null;
	this.vcardAssistantConfig = vcardAssistantConfig;
	this.resolve = resolve;
	this.reject = reject;
	this.windowManager = windowManager;
};
OO.inheritClass( mw.VcardAssistant.UI, OO.ui.ProcessDialog );

mw.VcardAssistant.UI.static.name = 'vcardAssistant';
mw.VcardAssistant.UI.static.size = 'larger';

/**
 * Reason why VcardAssistant has been exited
 *
 * @enum {string}
 */
mw.VcardAssistant.UI.ExitStatus = {
	/** Cancelled by user */
	CANCELLED: 'cancelled'
};

/**
 * Startup modes for VcardAssistant
 *
 * @enum {string}
 */
mw.VcardAssistant.UI.Mode = {
	/** Create a new vCard */
	CREATE: 'create',
	/** Edit or delete an existing vCard */
	EDIT: 'edit',
	/** Create a new vCard but don't inject and save it */
	TOOL: 'tool'
};

/**
 * @override
 * @see https://doc.wikimedia.org/oojs-ui/master/js/#!/api/OO.ui.ProcessDialog-method-initialize
 */
mw.VcardAssistant.UI.prototype.initialize = function () {
	mw.VcardAssistant.UI.parent.prototype.initialize.apply( this, arguments );

	this.$body.addClass( 'ext-vcardassistant' );

	this.editForm = new mw.VcardAssistant.UI.EditForm( this.vcardAssistantConfig, this.adaptations, this.prefill );
	this.previewForm = new mw.VcardAssistant.UI.PreviewForm( this.vcardAssistantConfig, this.editForm.parameters, this.vcardAssistantConfig.parameters.order, this.editForm.neverOmitParameters, this.editForm );
	this.actions.setMode( 'edit' );
	this.$body.html( this.editForm.$element );
	this.editForm.$element.added = true;
};

/**
 * @override
 * @see https://doc.wikimedia.org/oojs-ui/master/js/#!/api/OO.ui.ProcessDialog-method-getSetupProcess
 */
mw.VcardAssistant.UI.prototype.getSetupProcess = function ( data ) {
	return mw.VcardAssistant.UI.parent.prototype.getSetupProcess.call( this, data ).next( function () { // `this` needed!
		this.actions.setMode( 'edit' );
	}, this );
};

/**
 * @override
 * @see https://doc.wikimedia.org/oojs-ui/master/js/#!/api/OO.ui.ProcessDialog-method-getActionProcess
 */
mw.VcardAssistant.UI.prototype.getActionProcess = function ( action ) {
	const that = this;
	switch ( action ) {
		case 'close':
			return new OO.ui.Process( () => new Promise( ( resolve ) => {
				if ( this.editForm.countChanges() > 1 ) {
					const windowManager = new OO.ui.WindowManager();
					$( document.body ).append( windowManager.$element );

					const messageDialog = new OO.ui.MessageDialog();
					messageDialog.getActionProcess = ( dialogAction ) => {
						switch ( dialogAction ) {
							case 'close':
								return new OO.ui.Process( () => {
									this.close( {
										action: dialogAction
									} );
									this.reject( mw.VcardAssistant.UI.ExitStatus.CANCELLED );
									windowManager.destroy();
									resolve();
								} );
							case 'edit':
								return new OO.ui.Process( () => {
									windowManager.destroy();
									resolve();
								} );
						}
					};
					windowManager.addWindows( [ messageDialog ] );
					windowManager.openWindow( messageDialog, {
						title: mw.msg( 'va-msg-action-discard-title' ),
						message: mw.msg( 'va-msg-action-discard-message' ),
						actions: [
							{
								action: 'close',
								flags: [ 'destructive' ],
								icon: 'trash',
								label: mw.msg( 'va-msg-action-discard' ),
								title: mw.msg( 'va-msg-action-discard-description' )
							},
							{
								action: 'edit',
								flags: [ 'primary', 'safe' ],
								label: mw.msg( 'va-msg-action-returnToEdit' ),
								title: mw.msg( 'va-msg-action-returnToEdit-description' )
							}
						]
					} );
				} else {
					that.close( {
						action: action
					} );
					that.reject( mw.VcardAssistant.UI.ExitStatus.CANCELLED );
				}
			} ) );
		case 'copytoclipboard':
			return new OO.ui.Process( async () => {
				try {
					await navigator.clipboard.writeText( that.previewForm.getWikitext() );
				} catch ( ex ) {
					mw.notify( $( `<span>${mw.msg( 'va-msg-notify-errorCopyWikitextToClipboard' )}</span>` ), {
						tag: 'vcardassistant',
						title: 'vCard-Assistant',
						type: 'error'
					} );
					return;
				}

				mw.notify( $( `<span>${mw.msg( 'va-msg-notify-successCopyWikitextToClipboard' )}</span>` ), {
					tag: 'vcardassistant',
					title: 'vCard-Assistant'
				} );
				that.close( {
					action: action
				} );
				that.resolve( mw.VcardAssistant.UI.Result.tool( that.previewForm.getDisplayName(), that.previewForm.getWikitext(), that.previewForm.getParameters(), !that.editForm.validate() ) );
			} );
		case 'delete':
			return new OO.ui.Process( async () => {
				that.close( {
					action: action
				} );
				OO.ui.prompt( mw.msg( 'va-msg-prompt-delete-text' ), {
					textInput: {
						placeholder: mw.msg( 'va-msg-prompt-delete-input-placeholder' )
					}
				} ).done( ( result ) => {
					if ( result !== null ) {
						that.resolve( mw.VcardAssistant.UI.Result.delete( that.prefill !== null ? that.prefill[ that.vcardAssistantConfig.special_parameters.name ] : null, result ) );
					} else {
						that.open();
					}
				} );
			} );
		case 'help':
			return new OO.ui.Process( () => {
				window.open( that.vcardAssistantConfig.config.helpurl, '_blank' );
			} );
		case 'preview':
			return new OO.ui.Process( () => new Promise( ( resolve, reject ) => {
				async function moveToPreview() {
					that.actions.setMode( 'preview' );
					that.setSize( 'medium' );
					that.switchForm( that.previewForm );
					await that.previewForm.updatePreview();
				}

				if ( that.editForm.validate() ) {
					moveToPreview().then( resolve, reject );
				} else {
					const windowManager = new OO.ui.WindowManager();
					$( document.body ).append( windowManager.$element );

					const messageDialog = new OO.ui.MessageDialog();
					messageDialog.getActionProcess = ( dialogAction ) => {
						switch ( dialogAction ) {
							case 'edit':
								return new OO.ui.Process( () => {
									messageDialog.close( {
										action: dialogAction
									} );
									windowManager.destroy();
									resolve();
								} );
							case 'preview':
								return new OO.ui.Process( () => {
									messageDialog.close( {
										action: action
									} );
									windowManager.destroy();
									moveToPreview().then( resolve, reject );
								} );
						}
					};
					windowManager.addWindows( [ messageDialog ] );
					windowManager.openWindow( messageDialog, {
						title: mw.msg( 'va-msg-action-formatviolaction-title' ),
						message: mw.msg( 'va-msg-action-formatviolaction-description' ),
						actions: [
							{
								action: 'edit',
								flags: [ 'primary', 'safe' ],
								label: mw.msg( 'va-msg-action-returnToEdit' ),
								title: mw.msg( 'va-msg-action-returnToEdit-description' )
							},
							{
								action: 'preview',
								flags: [ 'progressive' ],
								label: mw.msg( 'va-msg-action-preview' ),
								title: mw.msg( 'va-msg-action-preview-description' )
							}
						]
					} );
				}
			} ) );
		case 'returntoedit':
			return new OO.ui.Process( async () => {
				that.actions.setMode( 'edit' );
				that.setSize( 'larger' );
				that.switchForm( that.editForm );
			} );
		case 'save':
			return new OO.ui.Process( () => {
				that.close( {
					action: action
				} );
				that.resolve( mw.VcardAssistant.UI.Result.edit( that.previewForm.getDisplayName(), that.previewForm.getWikitext(), that.previewForm.getParameters(), that.previewForm.getSummary(), that.previewForm.isMinorEdit(), !that.editForm.validate(), that.previewForm.isEditSectionWikitext() ) );
			} );
		default:
			mw.log.error( `[VcardAssistant] Unknown action: ${action}` );
			return mw.VcardAssistant.UI.parent.prototype.getActionProcess.call( this, action );
	}
};

/**
 * @override
 * @see https://doc.wikimedia.org/oojs-ui/master/js/#!/api/OO.ui.ProcessDialog-method-getBodyHeight
 */
mw.VcardAssistant.UI.prototype.getBodyHeight = function () {
	return 500;
};

/**
 * Hides the currently displayed form and displays newForm.
 *
 * @param {OO.ui.Widget} newForm
 * @return {void}
 */
mw.VcardAssistant.UI.prototype.switchForm = function ( newForm ) {
	this.$body.children().hide();
	this.$body.children().removeClass( 'switchform' );

	if ( typeof newForm.$element.added !== 'undefined' ) {
		newForm.$element.addClass( 'switchform' );
		newForm.$element.show();
	} else {
		newForm.$element.added = true;
		this.$body.append( newForm.$element );
	}

	if ( typeof newForm.focus === 'function' ) {
		newForm.focus();
	}
};

/**
 * Creates a new VcardAssitant dialog taking a configuration and a mode
 *
 * @static
 * @param {mw.VcardAssistant.UI.Mode} mode
 * @param {Object} vcardAssistantConfig Config to use for the created VcardAssistant
 * @param {mw.VcardAssistantConfig.Adaptations} adaptations Adapter for querying enums
 * @param {Object.<string, string>} prefill Initial value for parameters
 * @return {Promise}
 */
mw.VcardAssistant.UI.spawn = function ( mode, vcardAssistantConfig, adaptations, prefill ) {
	return new Promise( ( resolve, reject ) => {
		const windowManager = new OO.ui.WindowManager();
		$( document.body ).append( windowManager.$element );

		const dialog = new mw.VcardAssistant.UI( mode, windowManager, vcardAssistantConfig, ( result ) => {
			resolve( [ dialog, result ] );
		}, reject, adaptations, prefill );
		windowManager.addWindows( [
			dialog
		] );
		windowManager.openWindow( dialog );
	} );
};
