/**
 * @class
 * @constructor
 * @extends OO.ui.ComboBoxInputWidget
 * @extends OO.ui.mixin.LookupElement
 * @param {string} prefix
 * @param {string} namespace
 * @param {Object} [config] Configuration options.
 * @property {string} prefix
 * @property {string} namespace
 */
mw.VcardAssistant.UI.CommonsSearchField = function ( prefix, namespace, config ) {
	mw.VcardAssistant.UI.CommonsSearchField.super.call( this, config );
	OO.ui.mixin.LookupElement.call( this );

	this.prefix = prefix;
	this.namespace = namespace;
};
OO.inheritClass( mw.VcardAssistant.UI.CommonsSearchField, OO.ui.ComboBoxInputWidget );
OO.mixinClass( mw.VcardAssistant.UI.CommonsSearchField, OO.ui.mixin.LookupElement );

/**
 * Get a new request object of the current lookup query value.
 *
 * @override
 * @see https://doc.wikimedia.org/oojs-ui/master/js/#!/api/OO.ui.mixin.LookupElement-method-getLookupRequest
 * @return {jQuery.jqXHR} jQuery AJAX object, or promise object with an .abort() method
 */
mw.VcardAssistant.UI.CommonsSearchField.prototype.getLookupRequest = function () {
	return $.ajax( {
		data: {
			action: 'opensearch',
			format: 'json',
			namespace: this.namespace,
			origin: '*',
			redirects: 'resolve',
			search: this.getValue()
		},
		url: 'https://commons.wikimedia.org/w/api.php'
	} );
};

/**
 * Pre-process data returned by the request from #getLookupRequest.
 * The return value of this function will be cached, and any further queries for the given value will use the cache rather than doing API requests.
 *
 * @override
 * @see https://doc.wikimedia.org/oojs-ui/master/js/#!/api/OO.ui.mixin.LookupElement-method-getLookupCacheDataFromResponse
 * @param {Object} response Response from server
 * @return {Array<{data: string, label: string}>} Cached result data
 */
mw.VcardAssistant.UI.CommonsSearchField.prototype.getLookupCacheDataFromResponse = function ( response ) {
	this.searchresults = response[ 1 ];
	const searchResults = this.searchresults.map( ( result ) => {
		const name = result.indexOf( `${this.prefix}:` ) === 0 ? result.slice( `${this.prefix}:`.length ) : result;
		return {
			data: name,
			label: name
		};
	} );
	return searchResults;
};

/**
 * Get a list of menu option widgets from the (possibly cached) data returned by
 * #getLookupCacheDataFromResponse.
 *
 * @override
 * @see https://doc.wikimedia.org/oojs-ui/master/js/#!/api/OO.ui.mixin.LookupElement-method-getLookupMenuOptionsFromData
 * @param {Array} data Cached result data
 * @return {Array<mw.VcardAssistant.UI.SearchResult>} Menu items
 */
mw.VcardAssistant.UI.CommonsSearchField.prototype.getLookupMenuOptionsFromData = function ( data ) {
	return data.map( ( result ) => {
		return new mw.VcardAssistant.UI.SearchResult( result );
	} );
};

/**
 * Handle menu item "choose" event, updating the text input value to the value of the clicked item.
 *
 * @override
 * @see https://doc.wikimedia.org/oojs-ui/master/js/#!/api/OO.ui.mixin.LookupElement-method-onLookupMenuChoose
 * @param {OO.ui.MenuOptionWidget} item Selected item
 */
mw.VcardAssistant.UI.CommonsSearchField.prototype.onLookupMenuChoose = function ( item ) {
	this.setValue( item.getData() );
};
