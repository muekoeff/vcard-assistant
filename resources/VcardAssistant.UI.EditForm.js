/**
 * @class
 * @constructor
 * @extends OO.ui.Widget
 * @param {Object} vcardAssistantConfig Configuration for vCard-Assistant
 * @param {mw.VcardAssistantConfig.Adaptations} adaptations Resolver for wiki-specific data and logic
 * @param {Object.<string, string>} [prefill] Initial value for parameters
 * @property {mw.VcardAssistantConfig.Adaptations} adaptations Resolver for wiki-specific data and logic
 * @property {mw.VcardAssistant.UI.Parameter} lastEditParameter
 * @property {Array<string>} neverOmitParameters
 * @property {Object.<string, VcardAssistant.Parameter>} parameters
 * @property {Object.<string, string>} prefill Initial value for parameters
 * @property {Object} wikidataClaims
 */
mw.VcardAssistant.UI.EditForm = function ( vcardAssistantConfig, adaptations, prefill ) {
	/**
	 * @param {Object} catConfig
	 * @return {OO.ui.FieldsetLayout}
	 */
	function getCategory( catConfig ) {
		return new OO.ui.FieldsetLayout( {
			label: catConfig.label
		} );
	}

	mw.VcardAssistant.UI.EditForm.parent.call( this );

	this.adaptations = adaptations;
	this.neverOmitParameters = [];
	this.parameters = {};
	this.prefill = prefill ?? {};
	this.wikidataClaims = {};

	// Setup content panel
	const contentPanel = new OO.ui.PanelLayout( {
		classes: [ 'ext-vcardassistant-editform-contentpanel' ],
		expanded: true,
		padded: true
	} );
	const menuCategories = [];
	const triggers = [];

	// Not logged in warning
	if ( mw.config.get( 'wgUserName' ) === null ) {
		mw.loader.using( [ 'oojs-ui.styles.icons-user' ] );
		const notLogginPanel = new OO.ui.PanelLayout( {
			classes: [ 'usermessage', 'vcardassistant-warning' ],
			content: [
				new OO.ui.HorizontalLayout( {
					classes: [ 'vcardassistant-warning' ],
					expanded: false,
					items: [
						new OO.ui.IconWidget( {
							icon: 'userAnonymous'
						} ),
						new OO.ui.Widget( {
							classes: [ 'vcardassistant-warning-text' ],
							text: mw.msg( 'va-msg-warning-notLoggedIn' )
						} )
					]
				} )
			],
			expanded: false
		} );
		contentPanel.$element.append( notLogginPanel.$element );
	}

	// Outdated
	if ( vcardAssistantConfig.config.outdatedwarning === true && vcardAssistantConfig.special_parameters.lastupdate !== null && prefill?.[ vcardAssistantConfig.special_parameters.lastupdate ] ) {
		const lastupdate = this.adaptations.parseTimestamp( vcardAssistantConfig.special_parameters.lastupdate, prefill[ vcardAssistantConfig.special_parameters.lastupdate ] );
		const threshold = new Date( Date.now() - adaptations.getOutdatedThreshold( prefill ) );

		if ( lastupdate < threshold ) {
			mw.loader.using( [ 'oojs-ui.styles.icons-editing-advanced' ] );
			const outdatedPanel = new OO.ui.PanelLayout( {
				classes: [ 'usermessage', 'vcardassistant-warning' ],
				content: [
					new OO.ui.HorizontalLayout( {
						classes: [ 'vcardassistant-warning' ],
						expanded: false,
						items: [
							new OO.ui.IconWidget( {
								icon: 'calendar'
							} ),
							new OO.ui.Widget( {
								classes: [ 'vcardassistant-warning-text' ],
								text: mw.msg( 'va-msg-warning-outdated' )
							} )
						]
					} )
				],
				expanded: false
			} );
			contentPanel.$element.append( outdatedPanel.$element );
		}
	}

	// Setup EditForm categories and fields
	for ( const category of vcardAssistantConfig.parameters.categories ) {
		const categoryConfig = category.category;
		const rawParameters = category.params;
		const categoryFieldsetLayout = getCategory( categoryConfig );
		const fields = [];
		const menuItems = [];
		const parameters = [];

		for ( const parameterName of Object.keys( rawParameters ) ) {
			const parameter = new mw.VcardAssistant.UI.Parameter( vcardAssistantConfig, adaptations, parameterName, rawParameters[ parameterName ], prefill !== null ? prefill[ parameterName ] ?? null : null, this );
			const parameterField = parameter.getParameterInputField();
			parameters.push( parameter );
			fields.push( parameterField );

			if ( parameter.hidden ) {
				menuItems.push( $( '<div>' ).append( parameter.getParameterButton().$element ) );
			}

			// Is lastEdit?
			if ( parameter.type === 'lastedit' ) {
				this.lastEditParameter = parameter;
			}

			// Store
			this.parameters[ parameterName ] = parameter;

			// Never-omit parameters
			if ( parameter.neverOmit ) {
				this.neverOmitParameters.push( parameterName );
			}

			// Wikidata claims
			if ( parameter.wikidata !== null ) {
				this.wikidataClaims[ `${parameter.wikidata.property}${typeof parameter.wikidata.value !== 'undefined' ? `-${parameter.wikidata.value}` : ''}` ] = {
					config: parameter.wikidata,
					target: parameter.parameterName
				};
			}

			// Trigger
			if ( parameter.triggers !== null ) {
				for ( const trigger of parameter.triggers ) {
					triggers.push( {
						target: parameter,
						trigger: trigger
					} );
				}
			}
		}

		categoryFieldsetLayout.addItems( fields );
		contentPanel.$element.append( categoryFieldsetLayout.$element );

		const menuCategory = new mw.VcardAssistant.UI.MenuCategory( this, categoryConfig, menuItems, categoryFieldsetLayout );

		menuCategories.push( menuCategory );
	}

	// Process triggers
	for ( const triggerPair of triggers ) {
		triggerPair.target.setupTrigger( triggerPair.trigger, this.parameters );
	}

	// Create final layout and finish
	const menuPanel = new OO.ui.PanelLayout( {
		expanded: true,
		padded: true
	} );
	const $parameterMenu = $( '<div>' ).addClass( 'ext-vcardassistant-parameterlist' );

	for ( const menuCategory of menuCategories ) {
		$parameterMenu.append( menuCategory.$element );
	}

	menuPanel.$element.append( $parameterMenu );
	const menuLayout = new OO.ui.MenuLayout( {
		contentPanel: contentPanel,
		menuPanel: menuPanel
	} );

	// Load wikidata preview
	if ( prefill !== null && prefill[ vcardAssistantConfig.special_parameters.wikidata ] !== undefined ) {
		this.applyWikidataId( prefill[ vcardAssistantConfig.special_parameters.wikidata ] );
	}

	// Set position of menu
	$( window ).on( 'resize', () => {
		this.onWindowResize( menuLayout );
	} );
	this.onWindowResize( menuLayout );

	this.$element.append( menuLayout.$element );
};
OO.inheritClass( mw.VcardAssistant.UI.EditForm, OO.ui.Widget );

/**
 * @return {void}
 */
mw.VcardAssistant.UI.EditForm.prototype.initialize = function () {
	this.$element.find( '.fadein' ).removeClass( 'fadein' );
};

/**
 * Sets an input widget's value.
 *
 * @param {{string, Object}} parameter
 * @param {string} value
 * @return {void}
 */
mw.VcardAssistant.UI.EditForm.prototype.applyParameter = function ( parameter, value ) {
	this.parameters[ parameter ].setValue( value );
};

/**
 * Sets input widgets' values according to value in {@argument parameters}.
 *
 * @param {{string, Object}} parameters
 * @return {void}
 */
mw.VcardAssistant.UI.EditForm.prototype.applyParameters = function ( parameters ) {
	for ( const [ key, value ] of Object.entries( parameters ) ) {
		this.parameters[ key ].setValue( value );
	}
};

/**
 * Loads statements of the given Wikidata-entity-ID and applies them to the EditForm.
 *
 * @param {string} wikidataId Wikidata ID to load. Set to null to remove Wikidata previews.
 * @return {void}
 */
mw.VcardAssistant.UI.EditForm.prototype.applyWikidataId = async function ( wikidataId ) {
	/**
	 * @param {*} entity
	 * @param {*} wikidataConfig
	 * @return {{id: string, value: string}}
	 */
	function wikidataStatement( entity, wikidataConfig ) {
		if ( !entity || !entity.claims || !entity.claims[ wikidataConfig.property ] ) {
			return null;
		}

		const propertyObj = entity.claims[ wikidataConfig.property ];
		if ( !propertyObj || propertyObj.length < 1 || !propertyObj[ 0 ].mainsnak || !propertyObj[ 0 ].mainsnak.datavalue ) {
			return null;
		}

		let value;
		switch ( propertyObj[ 0 ].mainsnak.datavalue.type ) {
			case 'globecoordinate':
				if ( typeof wikidataConfig.value !== 'undefined' ) {
					value = propertyObj[ 0 ].mainsnak.datavalue.value[ wikidataConfig.value ];
				} else {
					value = `${propertyObj[ 0 ].mainsnak.datavalue.value.latitude}, ${propertyObj[ 0 ].mainsnak.datavalue.value.longitude}`;
				}
				break;
			case 'monolingualtext':
				value = propertyObj[ 0 ].mainsnak.datavalue.value.text;
				break;
			default:
				value = propertyObj[ 0 ].mainsnak.datavalue.value;
		}

		// Regex
		if ( typeof wikidataConfig.filter !== 'undefined' && typeof wikidataConfig.filter.regexp !== 'undefined' ) {
			value = value.match( wikidataConfig.filter.regexp )[ wikidataConfig.filter.group || 1 ];
		}

		return {
			id: propertyObj[ 0 ].id,
			value: value
		};
	}

	if ( wikidataId === null ) {
		for ( const propertyValue of Object.values( this.wikidataClaims ) ) {
			const parameterName = propertyValue.target;
			const parameter = this.parameters[ parameterName ];
			parameter.clearWikidata();
		}
	} else {
		const res = await $.ajax( {
			data: {
				action: 'wbgetentities',
				format: 'json',
				ids: wikidataId,
				redirects: 'yes',
				origin: '*',
				props: 'info|sitelinks|aliases|labels|descriptions|claims|datatype'
			},
			url: 'https://www.wikidata.org/w/api.php'
		} );

		if ( Object.keys( res.entities ).length > 0 ) {
			for ( const propertyValue of Object.values( this.wikidataClaims ) ) {
				const parameterName = propertyValue.target;
				const parameter = this.parameters[ parameterName ];
				const value = wikidataStatement( res.entities[ Object.keys( res.entities )[ 0 ] ], propertyValue.config );

				if ( value !== null ) {
					parameter.applyWikidata( value.value, value.id, wikidataId );
				}
			}
		}
	}
};

/**
 * @return {number} Number of edited parameters.
 */
mw.VcardAssistant.UI.EditForm.prototype.countChanges = function () {
	return Object.values( this.parameters ).reduce( ( accumulator, parameterValue ) => {
		return accumulator + ( parameterValue.wasChanged() ? 1 : 0 );
	}, 0 );
};

/**
 * @return {void}
 */
mw.VcardAssistant.UI.EditForm.prototype.getUpdateLastEdit = function () {
	console.assert( this.lastEditParameter !== null );
	return this.lastEditParameter.getUpdateLastEdit();
};

/**
 * @param {OO.ui.MenuLayout} menuLayout
 * @return {void}
 */
mw.VcardAssistant.UI.EditForm.prototype.onWindowResize = function ( menuLayout ) {
	if ( $( window ).width() > 600 ) {
		menuLayout.setMenuPosition( 'before' );
	} else {
		menuLayout.setMenuPosition( 'bottom' );
	}
};

/**
 * @param {string} value
 * @return {void}
 */
mw.VcardAssistant.UI.EditForm.prototype.setUpdateLastEdit = function ( value ) {
	console.assert( this.lastEditParameter !== null );
	this.lastEditParameter.setUpdateLastEdit( value );
};

/**
 * @return {boolean} Whether all parameters are empty or valid.
 */
mw.VcardAssistant.UI.EditForm.prototype.validate = function () {
	return Object.values( this.parameters ).reduce( ( accumulator, parameterValue ) => {
		return accumulator && parameterValue.validate( true );
	}, true );
};
