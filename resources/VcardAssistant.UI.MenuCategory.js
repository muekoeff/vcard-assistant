/**
 * @class
 * @constructor
 * @extends OO.ui.Widget
 * @param {mw.VcardAssistant.UI.EditForm} editForm Form this category is attached to.
 * @param {Object} category
 * @param {Object} menuItems
 * @param {OO.ui.FieldsetLayout} categoryFieldsetLayout
 * @param {Object} [config]
 * @property {Object} category
 * @property {mw.VcardAssistant.UI.EditForm} editForm
 * @property {Object} menuItems
 * @property {OO.ui.FieldsetLayout} categoryFieldsetLayout
 */
mw.VcardAssistant.UI.MenuCategory = function ( editForm, category, menuItems, categoryFieldsetLayout, config ) {
	mw.VcardAssistant.UI.MenuCategory.super.call( this, config );

	this.editForm = editForm;
	this.category = category;
	this.menuItems = menuItems;
	this.categoryFieldsetLayout = categoryFieldsetLayout;

	const $title = $( `<span class="ext-vcardassistant-menu-categorylabel">${category.label}</span>` );
	$title.on( 'click', ( e ) => {
		e.preventDefault();
		categoryFieldsetLayout.$element[ 0 ].scrollIntoView( {
			behavior: 'smooth'
		} );
	} );
	this.$element.append( $title );
	this.$element.append( menuItems );
};
OO.inheritClass( mw.VcardAssistant.UI.MenuCategory, OO.ui.Widget );
