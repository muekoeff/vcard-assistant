/**
 * @class
 * @constructor
 * @extends OO.ui.MenuOptionWidget
 * @extends OO.ui.ComboBoxInputWidget
 * @param {mw.VcardAssistant.UI.EditForm} editForm The form that this field is attached to
 * @param {Object} [config] Configuration options
 * @property {mw.VcardAssistant.UI.EditForm} editForm
 */
mw.VcardAssistant.UI.WikidataSearchField = function ( editForm, config ) {
	this.editForm = editForm;

	mw.VcardAssistant.UI.WikidataSearchField.super.call( this, config );
	OO.ui.mixin.LookupElement.call( this );

	this.on( 'change', () => {
		this.editForm.applyWikidataId( null );
	} );
};
OO.inheritClass( mw.VcardAssistant.UI.WikidataSearchField, OO.ui.ComboBoxInputWidget );
OO.mixinClass( mw.VcardAssistant.UI.WikidataSearchField, OO.ui.mixin.LookupElement );

/**
 * @override
 * @see https://doc.wikimedia.org/oojs-ui/master/js/#!/api/OO.ui.mixin.LookupElement-method-getLookupRequest
 * @return {jQuery.jqXHR}
 */
mw.VcardAssistant.UI.WikidataSearchField.prototype.getLookupRequest = function () {
	return $.ajax( {
		data: {
			action: 'wbsearchentities',
			format: 'json',
			language: mw.config.get( 'wgUserLanguage' ),
			limit: 20,
			origin: '*',
			search: this.getValue(),
			type: 'item',
			uselang: mw.config.get( 'wgUserLanguage' )
		},
		url: 'https://www.wikidata.org/w/api.php'
	} );
};

/**
 * Pre-process data returned by the request from #getLookupRequest.
 * The return value of this function will be cached, and any further queries for the given value will use the cache rather than doing API requests.
 *
 * @override
 * @see https://doc.wikimedia.org/oojs-ui/master/js/#!/api/OO.ui.mixin.LookupElement-method-getLookupCacheDataFromResponse
 * @param {Object} response Response from server
 * @return {Array} Cached result data
 */
mw.VcardAssistant.UI.WikidataSearchField.prototype.getLookupCacheDataFromResponse = function ( response ) {
	this.searchresults = response.search;
	const searchResults = this.searchresults.map( ( result ) => {
		return {
			data: result.id,
			label: result.label,
			description: result.description
		};
	} );
	return searchResults;
};

/**
 * Get a list of menu option widgets from the (possibly cached) data returned by
 * #getLookupCacheDataFromResponse.
 *
 * @override
 * @see https://doc.wikimedia.org/oojs-ui/master/js/#!/api/OO.ui.mixin.LookupElement-method-getLookupMenuOptionsFromData
 * @param {Array} data Cached result data
 * @return {OO.ui.MenuOptionWidget[]} Menu items
 */
mw.VcardAssistant.UI.WikidataSearchField.prototype.getLookupMenuOptionsFromData = function ( data ) {
	return data.map( ( result ) => {
		return new mw.VcardAssistant.UI.SearchResult( result );
	} );
};

/**
 * Handle menu item "choose" event, updating the text input value to the value of the clicked item.
 *
 * @override
 * @see https://doc.wikimedia.org/oojs-ui/master/js/#!/api/OO.ui.mixin.LookupElement-method-onLookupMenuChoose
 * @param {OO.ui.MenuOptionWidget} item Selected item
 * @return {void}
 */
mw.VcardAssistant.UI.WikidataSearchField.prototype.onLookupMenuChoose = function ( item ) {
	this.setValue( item.getData() );
	this.editForm.applyParameter( 'auto', 1 );
	this.editForm.applyWikidataId( item.getData() );
};
