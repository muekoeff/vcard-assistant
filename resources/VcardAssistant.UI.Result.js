/**
 * @class
 * @constructor
 * @param {mw.VcardAssistant.UI.Result.Action} action
 * @param {string} displayName
 * @property {mw.VcardAssistant.UI.Result.Action} action
 * @property {string} displayName
 * @property {boolean} editSectionWikitext
 * @property {boolean} hasFormatErrors
 * @property {Object.<string, Object>} parameters
 * @property {boolean} minorChanges
 * @property {string} summary
 * @property {string} wikitext
 */
mw.VcardAssistant.UI.Result = function ( action, displayName ) {
	this.action = action;
	this.displayName = displayName;
};

/**
 * @enum
 */
mw.VcardAssistant.UI.Result.Action = {
	DELETE: 'delete',
	EDIT: 'edit'
};

/**
 * @static
 * @param {string} displayName
 * @param {string} summary
 * @return {mw.VcardAssistant.UI.Result}
 */
mw.VcardAssistant.UI.Result.delete = function ( displayName, summary ) {
	const result = new mw.VcardAssistant.UI.Result( mw.VcardAssistant.UI.Result.Action.DELETE, displayName );
	result.summary = summary;

	return result;
};

/**
 * @static
 * @param {string} displayName
 * @param {string} wikitext
 * @param {Object.<string, Object>} parameters
 * @param {string} summary
 * @param {boolean} isMinorEdit
 * @param {boolean} hasFormatErrors
 * @param {boolean} editSectionWikitext
 * @return {mw.VcardAssistant.UI.Result}
 */
mw.VcardAssistant.UI.Result.edit = function ( displayName, wikitext, parameters, summary, isMinorEdit, hasFormatErrors, editSectionWikitext ) {
	const result = new mw.VcardAssistant.UI.Result( mw.VcardAssistant.UI.Result.Action.EDIT, displayName );

	result.wikitext = wikitext;
	result.parameters = parameters;
	result.summary = summary;
	result.editSectionWikitext = editSectionWikitext;
	result.isMinorEdit = isMinorEdit;
	result.hasFormatErrors = hasFormatErrors;

	return result;
};

/**
 * @static
 * @param {string} displayName
 * @param {string} wikitext
 * @param {Object.<string, Object>} parameters
 * @param {boolean} hasFormatErrors
 * @return {mw.VcardAssistant.UI.Result}
 */
mw.VcardAssistant.UI.Result.tool = function ( displayName, wikitext, parameters, hasFormatErrors ) {
	const result = new mw.VcardAssistant.UI.Result( mw.VcardAssistant.UI.Result.Action.EDIT, displayName );

	result.wikitext = wikitext;
	result.parameters = parameters;
	result.hasFormatErrors = hasFormatErrors;

	return result;
};
