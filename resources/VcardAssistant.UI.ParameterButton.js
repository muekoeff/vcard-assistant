/**
 * @class
 * @constructor
 * @extends OO.ui.ButtonWidget
 * @param {mw.VcardAssistant.UI.EditForm} editForm
 * @param {mw.VcardAssistant.UI.Parameter} parameter
 * @param {boolean} initiallyVisible Whether this parameter should be initially hidden or shown
 * @param {Object} [config]
 * @property {mw.VcardAssistant.UI.EditForm} editForm
 * @property {mw.VcardAssistant.UI.Parameter} parameter
 */
mw.VcardAssistant.UI.ParameterButton = function ( editForm, parameter, initiallyVisible, config ) {
	config = $.extend( {
		icon: 'add',
		framed: false,
		flags: 'progressive'
	}, config );
	mw.VcardAssistant.UI.ParameterButton.super.call( this, config );

	this.editForm = editForm;
	this.parameter = parameter;
	this.setLabel( parameter.label );
	this.toggleAction( initiallyVisible );
	this.connect( this, {
		click: 'toggleAction'
	} );
};
OO.inheritClass( mw.VcardAssistant.UI.ParameterButton, OO.ui.ButtonWidget );

/**
 * Toggle the parameter's field's visibility, and tell the add/remove all button to update itself accordingly.
 *
 * @param {boolean} [show] Whether to make parameter visible
 * @return {void}
 */
mw.VcardAssistant.UI.ParameterButton.prototype.toggleAction = function ( show ) {
	if ( show === undefined ) {
		show = !this.isEnabled;
	}

	if ( show ) {
		this.setIcon( 'subtract' );
		this.setFlags( {
			destructive: true,
			progressive: false
		} );
		this.setTitle( mw.msg( 'va-msg-parameterButton-remove' ) );
		this.parameter.toggle( true );
		this.parameter.inputFieldLayoutWidget.$element.addClass( 'fadein' );

		this.parameter.inputFieldLayoutWidget.$element[ 0 ].scrollIntoView( {
			behavior: 'smooth'
		} );
	} else {
		this.setIcon( 'add' );
		this.setFlags( {
			progressive: true,
			destructive: false
		} );
		this.setTitle( mw.msg( 'va-msg-parameterButton-add' ) );
		this.parameter.toggle( false );
		this.parameter.inputFieldLayoutWidget.$element.removeClass( 'fadein' );
	}

	this.isEnabled = show;
};
