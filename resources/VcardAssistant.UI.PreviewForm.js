/**
 * @class
 * @constructor
 * @extends OO.ui.Widget
 * @param {Object} vcardAssistantConfig
 * @param {Array<mw.VcardAssistant.UI.Parameter>} parameters
 * @param {Array<string>} parameterOrder
 * @param {Array<string>} neverOmitParameters
 * @param {mw.VcardAssistant.UI.EditForm} editForm
 * @property {mw.VcardAssistant.UI.EditForm} editForm
 * @property {OO.ui.CheckboxInputWidget} editSectionWikitextCheckboxInputWidget
 * @property {OO.ui.CheckboxInputWidget} lastUpdateCheckboxInputWidget
 * @property {OO.ui.FieldLayout} lastUpdateFieldLayout
 * @property {OO.ui.CheckboxInputWidget} minorEditCheckboxInputWidget
 * @property {Array<string>} neverOmitParameters
 * @property {Array<mw.VcardAssistant.UI.Parameter>} parameters
 * @property {Array<string>} parameterOrder
 * @property {Object} vcardAssistantConfig
 */
mw.VcardAssistant.UI.PreviewForm = function ( vcardAssistantConfig, parameters, parameterOrder, neverOmitParameters, editForm ) {
	mw.VcardAssistant.UI.PreviewForm.parent.call( this );

	this.editForm = editForm;
	this.neverOmitParameters = neverOmitParameters;
	this.parameters = parameters;
	this.parameterOrder = parameterOrder;
	this.vcardAssistantConfig = vcardAssistantConfig;

	// Setup content panel
	const contentPanel = new OO.ui.StackLayout( {
			classes: [ 'ext-vcardassistant-previewform-contentpanel' ],
			expanded: true,
			padded: true
		} ),
		// Setup upper code & preview panel
		previewPanel = new OO.ui.PanelLayout( {
			classes: [ 'ext-vcardassistant-previewform-previewpanel' ],
			expanded: false
		} );
	previewPanel.$element.append( ( new OO.ui.LabelWidget( {
		label: mw.msg( 'va-msg-preview-title' )
	} ) ).$element );
	this.previewPanel = new OO.ui.PanelLayout( {
		classes: [ 'ext-vcardassistant-previewform-previewpanel' ],
		framed: true,
		expanded: false,
		padded: false
	} );
	previewPanel.$element.append( this.previewPanel.$element );
	this.mediawikiCodeInputWidget = new OO.ui.MultilineTextInputWidget( {
		rows: 5
	} );
	this.mediawikiCodeInputWidget.$input.attr( 'readonly', true );
	previewPanel.$element.append( ( new OO.ui.FieldLayout( this.mediawikiCodeInputWidget, {
		align: 'top',
		label: mw.msg( 'va-msg-preview-generatedCode-title' )
	} ) ).$element );
	contentPanel.$element.append( previewPanel.$element );

	// Setup lower submit panel
	const submitPanel = new OO.ui.PanelLayout( {
		classes: [ 'ext-vcardassistant-previewform-submitpanel' ],
		expanded: false,
		padded: false
	} );
	this.summaryInputWidget = new OO.ui.TextInputWidget();
	this.lastUpdateCheckboxInputWidget = new OO.ui.CheckboxInputWidget( {
		selected: false,
		value: 'update-lastedit'
	} );
	this.lastUpdateCheckboxInputWidget.on( 'change', ( value ) => {
		this.editForm.setUpdateLastEdit( value );
		this.updatePreview();
	} );
	this.lastUpdateFieldLayout = new OO.ui.FieldLayout( this.lastUpdateCheckboxInputWidget, {
		align: 'inline',
		label: mw.msg( 'va-msg-preview-action-markAsUpdated' )
	} );
	this.minorEditCheckboxInputWidget = new OO.ui.CheckboxInputWidget( {
		value: 'minoredit'
	} );
	this.editSectionWikitextCheckboxInputWidget = new OO.ui.CheckboxInputWidget( {
		value: 'editsectionwikitext'
	} );
	const summaryFieldsetLayout = new OO.ui.FieldsetLayout( {
		classes: [ 'ext-vcardassistant-previewform-summaryfieldset' ],
		label: null,
		items: [
			new OO.ui.FieldLayout( this.summaryInputWidget, {
				align: 'top',
				label: mw.msg( 'va-msg-preview-summary' )
			} ),
			new OO.ui.FieldLayout( new OO.ui.Widget( {
				content: [
					new OO.ui.HorizontalLayout( {
						items: [
							this.lastUpdateFieldLayout,
							new OO.ui.FieldLayout( this.minorEditCheckboxInputWidget, {
								align: 'inline',
								label: mw.msg( 'va-msg-preview-action-minorChanges' )
							} ),
							new OO.ui.FieldLayout( this.editSectionWikitextCheckboxInputWidget, {
								align: 'inline',
								label: mw.msg( 'va-msg-preview-action-editSectionWikitext' )
							} )
						]
					} )
				]
			} ), {
				align: 'top',
				label: null
			} )
		]
	} );

	submitPanel.$element.append( summaryFieldsetLayout.$element );
	submitPanel.$element.append( ( new OO.ui.HtmlSnippet( `<div class="ext-vcardassistant-legal">${this.vcardAssistantConfig.config.submitLegal}</div>` ) ).toString() );
	contentPanel.$element.append( submitPanel.$element );

	this.$element.append( contentPanel.$element );
};
OO.inheritClass( mw.VcardAssistant.UI.PreviewForm, OO.ui.Widget );

/**
 * @return {void}
 */
mw.VcardAssistant.UI.PreviewForm.prototype.initialize = async function () {
	if ( this.editForm.getUpdateLastEdit() === null ) {
		this.lastUpdateFieldLayout.toggle( false );
	} else {
		this.lastUpdateCheckboxInputWidget.setSelected( this.editForm.getUpdateLastEdit() );
	}

	await this.updatePreview();
};

/**
 * @return {void}
 */
mw.VcardAssistant.UI.PreviewForm.prototype.focus = function () {
	this.summaryInputWidget.focus();
};

/**
 * @return {string}
 */
mw.VcardAssistant.UI.PreviewForm.prototype.getDisplayName = function () {
	return this.parameters[ this.vcardAssistantConfig.special_parameters.name ].getValue();
};

/**
 * @return {Array<{parameter: string, value: string}>}
 */
mw.VcardAssistant.UI.PreviewForm.prototype.getParameters = function () {
	const parameterValueMap = [];
	for ( const [ parameterName, parameter ] of Object.entries( this.parameters ) ) {
		let value;
		if ( parameter.hidden ) {
			value = '';
		} else {
			value = parameter.getValue();
		}
		if ( !parameter.excluded && ( value !== '' || this.neverOmitParameters.includes( parameterName ) ) ) {
			parameterValueMap.push( {
				parameter: parameterName,
				prefill: parameter.prefill,
				value: value.trim()
			} );
		}
	}

	const sortedParameterValueMap = parameterValueMap.sort( ( a, b ) => {
		return this.parameterOrder.indexOf( a.parameter ) - this.parameterOrder.indexOf( b.parameter );
	} );
	return sortedParameterValueMap;
};

/**
 * @return {string}
 */
mw.VcardAssistant.UI.PreviewForm.prototype.getSummary = function () {
	return this.summaryInputWidget.getValue();
};

/**
 * @return {string}
 */
mw.VcardAssistant.UI.PreviewForm.prototype.getWikitext = function () {
	const parameters = this.getParameters();
	let parameterString = '';
	if ( parameters.length > 0 ) {
		parameterString = ` |${parameters.map( ( parameter ) => {
			return `${parameter.parameter}=${parameter.value}`;
		} ).join( ' |' )}`;
	}
	return `{{vCard${parameterString}\n}}`;
};

/**
 * @return {boolean}
 */
mw.VcardAssistant.UI.PreviewForm.prototype.isEditSectionWikitext = function () {
	return this.editSectionWikitextCheckboxInputWidget.isSelected();
};

/**
 * @return {boolean}
 */
mw.VcardAssistant.UI.PreviewForm.prototype.isMinorEdit = function () {
	return this.minorEditCheckboxInputWidget.isSelected();
};

/**
 * @return {Promise}
 */
mw.VcardAssistant.UI.PreviewForm.prototype.updatePreview = async function () {
	const wikitext = this.getWikitext();

	this.mediawikiCodeInputWidget.setValue( wikitext ).adjustSize();
	this.previewPanel.$element.html( `<i>${mw.msg( 'va-msg-preview-prepare' )}</i>` );

	const progressBarWidget = new OO.ui.ProgressBarWidget( {
		progress: false
	} );
	this.previewPanel.$element.append( progressBarWidget.$element );

	const e = await $.ajax( {
		data: {
			action: 'parse',
			format: 'json',
			prop: 'text',
			contentmodel: 'wikitext',
			text: wikitext
		},
		method: 'GET',
		url: mw.util.wikiScript( 'api' )
	} );

	this.previewPanel.$element.html( e.parse.text[ '*' ] );
	this.previewPanel.$element.find( 'a' ).each( () => {
		if ( $( this ).attr( 'target' ) === undefined ) {
			$( this ).attr( 'target', '_blank' );
		} else if ( !$( this ).attr( 'target' ).includes( '_blank ' ) ) {
			$( this ).attr( 'target', `${$( this ).attr( 'target' )} _blank` );
		}
	} );
};
