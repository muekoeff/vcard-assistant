printf '%s\n%s\n%s\n%s\n%s\n%s\n%s\n' \
	"//<nowiki>" \
	"/** vCard-Assistant" \
	" * Source code: https://gitlab.com/muekoeff/vcard-assistant" \
	" * Please report issues and requests preferably at https://gitlab.com/muekoeff/vcard-assistant/-/issues" \
	" */" \
	"$(cat dist/VcardAssistant.bundle.js)" \
	"//</nowiki>" > dist/VcardAssistant.bundle.js
printf '%s\n%s\n%s\n%s\n%s\n%s\n%s\n' \
	"//<nowiki>" \
	"/** vCard-Assistant" \
	" * Source code: https://gitlab.com/muekoeff/vcard-assistant" \
	" * Please report issues and requests preferably at https://gitlab.com/muekoeff/vcard-assistant/-/issues" \
	" */" \
	"$(cat dist/VcardAssistantVirion.bundle.js)" \
	"//</nowiki>" > dist/VcardAssistantVirion.bundle.js