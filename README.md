# vCard-Assistant
Project by [nw520](https://de.wikivoyage.org/wiki/Benutzer:Nw520) aiming to create a new user interface for the *ListingEditor* used by the German Wikivoyage. It uses the [OOUI](https://www.mediawiki.org/wiki/OOUI) framework created by Wikimedia.

Would you like to report a problem or make suggestions?
* If you have a GitLab account please open an [issue](https://gitlab.com/muekoeff/vcard-assistant/-/issues).
* Otherwise please leave a message [on the discussion page]({@TODO: Discussion link}) in {@TODO: Wiki}.

This project can be split in roughly two parts:
* **VcardAssistant**. The actual dialog for editing vCards with all its logic.
* **VcardAssistantVirion**. The component which analyses a wiki article and then loads the actual vCard-Assistant.

## Customisation for different Wikis
There are two files which can be used to configure *vCard-Assistant*:

### `externals/vcard-assistant.config.json`
This JSON file contains basic (= static) configuration options like supported parameters and should be edited for each wiki.

Please refer to the [JSON schema file](https://muekoeff.gitlab.io/vcard-assistant/docs/schemas/vcard-assistant.config.schema.json) for the vCard-Assistant configuration format.

### `externals/VcardAssistantConfig.Adaptations.js`
This file contains more advanced configuration options like defining custom enumerations.

## Settings for `common.js`
vCard-Assistant's behaviour can be changed by specifying properties for `window.vcardassistantconfig`.

One could for example add this to their common.js or simply type it into their browser console.
```JavaScript
window.vcardassistantconfig = {
	virion_mainloaded: true
}
```

| Parameter	| Description	|
| ---------	| -------------	|
| `virion_mainloaded`	| Set to `true`, to assume main script as loaded in Virion. |

## Dependencies
* `ext.kartographer.box`
* `mediawiki.action.view.postEdit`
* `mediawiki.notify`
* `mediawiki.util`
* `oojs`
* `oojs-ui`
* `oojs-ui.styles.icons-accessibility`
* `oojs-ui.styles.icons-editing-advanced`
* `oojs-ui.styles.icons-editing-core`
* `oojs-ui.styles.icons-location`
* `oojs-ui.styles.icons-moderation`
* `oojs-ui.styles.icons-user`

## Build
You first need to download all required dependencies by running `npm install`. Afterwards the project can be built by running `npm run build`.

## How to debug strings
Message keys can be displayed by using the query parameter `uselang=qqx`.

## Architecture
### Editing an existing vCard
```plantuml
@startuml

hide footbox
actor User

User -> VcardAssistantVirion: edit existing vCard

... load dependencies, etc. ...

VcardAssistantVirion -> VcardAssistant: editVcard()
activate VcardAssistant

VcardAssistant -> AJAX.mw: get wikitext for section
activate AJAX.mw
AJAX.mw --> VcardAssistant: Promise
AJAX.mw -> VcardAssistant: Promise resolved: wikitext
deactivate AJAX.mw

VcardAssistant --> VcardAssistant: stripComments(): encode comments
VcardAssistant --> VcardAssistant: getVcardInSection(): get vCard wikitext and position
VcardAssistant --> VcardAssistant: wikitextToParameterMap(): wikitext to parameter map

create VcardAssistant.UI
VcardAssistant -> VcardAssistant.UI: spawn()
activate VcardAssistant.UI
VcardAssistant.UI --> VcardAssistant: Promise
VcardAssistant.UI -> User: request input
User --> VcardAssistant.UI: finishes
VcardAssistant.UI -> VcardAssistant: Promise resolved: VcardAssistant.UI.Result
deactivate VcardAssistant.UI
destroy VcardAssistant.UI

alt user requested to edit section text
    VcardAssistant -> VcardAssistant: display dialog
    activate VcardAssistant
    VcardAssistant -> User: request input
    User --> VcardAssistant: new section text
    VcardAssistant --> VcardAssistant: new section text
    deactivate VcardAssistant
end

VcardAssistant -> VcardAssistant: replace vCard-wikitext in section text by pre/postpending new vCard-wikitext

create mw.Api
VcardAssistant -> mw.Api: save new section text
mw.Api --> VcardAssistant: Promise
mw.Api -> VcardAssistant: Promise resolved
destroy mw.Api

VcardAssistant -> VcardAssistantVirion
deactivate VcardAssistant
VcardAssistantVirion -> User: reload page

@enduml
```