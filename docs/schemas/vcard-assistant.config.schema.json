{
	"$schema": "http://json-schema.org/draft-07/schema#",
	"definitions": {
		"Category": {
			"title": "Category",
			"description": "A category bundles one or more parameters. Can be empty, though that would be kinda stupid.",
			"type": "object",
			"additionalProperties": false,
			"properties": {
				"category": {
					"$ref": "#/definitions/CategoryMeta"
				},
				"params": {
					"description": "Key must equal parameter name as used in the template.",
					"type": "object",
					"properties": {
						"/": {}
					},
					"patternProperties": {
						".*": {
							"$ref": "#/definitions/Parameter"
						}
					}
				}
			},
			"required": [
				"category",
				"params"
			]
		},
		"CategoryMeta": {
			"title": "Category meta",
			"type": "object",
			"additionalProperties": false,
			"properties": {
				"id": {
					"description": "Unique id for this category. Only used for internal purposes.",
					"type": "string"
				},
				"label": {
					"description": "Display name for this category.",
					"type": "string"
				}
			},
			"required": [
				"id",
				"label"
			]
		},
		"ConditionsList": {
			"description": "List of conditions which all have to be fullfilled.",
			"type": "array",
			"items": {
				"type": "object"
			}
		},
		"Enum": {
			"title": "Enumeration",
			"description": "If string then the EnumerationResolver will be queried with the string as search key. Otherwise if an array then its items will be used.",
			"anyOf": [
				{
					"description": "List of enumeration items.",
					"type": "array",
					"items": {
						"$ref": "#/definitions/EnumItem"
					}
				},
				{
					"description": "Key to use for querying the EnumerationResolver.",
					"type": "string"
				}
			]
		},
		"EnumItem": {
			"title": "Enumeration item",
			"description": "An item which is part of an enumaration.",
			"anyOf": [
				{
					"type": "object",
					"properties": {
						"color": {
							"type": "string"
						},
						"label": {
							"type": "string"
						},
						"data": {
							"type": "string"
						}
					},
					"required": [
						"label",
						"data"
					]
				},
				{
					"description": "Value of this item.",
					"type": "string"
				}
			]
		},
		"Format": {
			"title": "Format",
			"description": "Defines rules for what an user input must look like.",
			"properties": {
				"regexp": {
					"description": "RegExp which has to be satisfied by the input in order for it to be accepted.",
					"type": "string"
				},
				"message": {
					"description": "Message which shall be displayed if RegExp is not satisfied.",
					"type": "string"
				}
			},
			"required": [
				"regexp"
			]
		},
		"Parameter": {
			"title": "Parameter",
			"allOf": [
				{
					"$ref": "#/definitions/ParameterBasic"
				},
				{
					"oneOf": [
						{
							"description": "enum-parameter specific configurations.",
							"type": "object",
							"properties": {
								"type": {
									"type": "string",
									"pattern": "^enum$"
								},
								"enum": {
									"description": "List of allowed values.",
									"$ref": "#/definitions/Enum"
								},
								"multiple": {
									"description": "Set to true if multiple values may be chosen.",
									"type": "boolean"
								}
							},
							"required": [
								"enum"
							]
						},
						{
							"description": "string-parameter specific configurations.",
							"type": "object",
							"properties": {
								"type": {
									"type": "string",
									"pattern": "^string$"
								},
								"multiline": {
									"description": "Whether the user may use newlines. Will display a larger textbox in UI.",
									"type": "integer"
								}
							}
						},
						{
							"description": "number-parameter specific configurations.",
							"type": "object",
							"properties": {
								"type": {
									"type": "string",
									"pattern": "^number$"
								},
								"max": {
									"description": "Maximum value a user may specify.",
									"type": "integer"
								},
								"min": {
									"description": "Minimum value a user may specify.",
									"type": "integer"
								}
							}
						},
						{
							"type": "object",
							"properties": {
								"type": {
									"type": "string",
									"pattern": "^(commons-category|commons-file|lastedit|phone|toggle|url|wikidata-item-id)$"
								}
							}
						}
					]
				}
			]
		},
		"ParameterBasic": {
			"title": "Basic config for parameters",
			"type": "object",
			"additionalProperties": true,
			"properties": {
				"label": {
					"description": "Label to display in UI.",
					"type": "string"
				},
				"description": {
					"description": "Description to display as help.",
					"type": "string"
				},
				"example": {
					"description": "Example value",
					"type": "string"
				},
				"neverOmit": {
					"description": "Set to true if this parameter should always be included in the generated MediaWiki template code, even if no value for this parameter is provided.",
					"type": "boolean"
				},
				"type": {
					"description": "Type of this parameter. See documentation for allowed values.",
					"type": "string"
				},
				"required": {
					"description": "Set to true if the user should be warned if no value is provided for this parameter. Note that this won't force the user to provide a value.",
					"type": "boolean"
				},
				"hidden": {
					"description": "Whether to initially hide this parameter so that a user has to select it in the sidebar in order to set a value.",
					"type": "boolean"
				},
				"wikidata": {
					"description": "Associated Wikidata entity to use for previewing.",
					"$ref": "#/definitions/Wikidata"
				},
				"format": {
					"description": "Regular expression for values of this parameter. Please note that empty values are always allowed as long as `required` is not set to true",
					"type": "array",
					"items": {
						"$ref": "#/definitions/Format"
					}
				},
				"preview": {
					"description": "How a preview button is generated for this parameter",
					"oneOf": [
						{
							"type": "object",
							"properties": {
								"type": {
									"type": "string",
									"pattern": "^url$"
								},
								"url": {
									"description": "Either a single URL or a list of multiple definitions. {parameter} and {encodedparameter} can be used as placeholders for the parameter value",
									"oneOf": [
										{
											"type": "string"
										},
										{
											"type": "object",
											"properties": {
												"conditions":  {
													"$ref": "#/definitions/ConditionsList"
												},
												"url": {
													"type": "string"
												}
											}
										}
									]
								}
							},
							"required": [
								"type",
								"url"
							]
						},
						{
							"type": "object",
							"properties": {
								"type": {
									"type": "string",
									"pattern": "^coordinates$"
								},
								"lat": {
									"description": "Name of parameter for latitude. Use $this if that's the current parameter",
									"type": "string"
								},
								"long": {
									"description": "Name of parameter for longitude. Use $this if that's the current parameter",
									"type": "string"
								}
							},
							"required": [
								"type",
								"lat",
								"long"
							]
						}
					]
				},
				"triggers": {
					"description": "Action to perform as soon as certain conditions are fullfilled.",
					"type": "array",
					"items": {
						"$ref": "#/definitions/Trigger"
					}
				}
			},
			"required": [
				"label",
				"type"
			]
		},
		"Trigger": {
			"type": "object",
			"additionalProperties": false,
			"properties": {
				"conditions": {
					"$ref": "#/definitions/ConditionsList"
				},
				"action": {
					"description": "Action to perform once all conditions are fullfilled.",
					"type": "object"
				}
			},
			"required": [
				"conditions",
				"action"
			]
		},
		"TriggerAction": {
			"oneOf": [
				{
					"$ref": "#/definitions/TriggerAction_Excluded"
				},
				{
					"$ref": "#/definitions/TriggerAction_Value"
				},
				{
					"$ref": "#/definitions/TriggerAction_Visible"
				}
			]
		},
		"TriggerAction_Excluded": {
			"type": "object",
			"additionalProperties": false,
			"properties": {
				"excluded": {
					"description": "Whether to exclude this parameter from output.",
					"type": "boolean"
				}
			},
			"required": [
				"excluded"
			]
		},
		"TriggerAction_Value": {
			"type": "object",
			"additionalProperties": false,
			"properties": {
				"value": {
					"description": "Value to set.",
					"type": "string"
				}
			},
			"required": [
				"value"
			]
		},
		"TriggerAction_Visible": {
			"type": "object",
			"additionalProperties": false,
			"properties": {
				"visible": {
					"description": "Whether to display this parameter in the UI. Note that hiding it won't exclude it from being output.",
					"type": "boolean"
				}
			},
			"required": [
				"visible"
			]
		},
		"TriggerCondition": {
			"oneOf": [
				{
					"$ref": "#/definitions/TriggerCondition_Value"
				}
			]
		},
		"TriggerCondition_Value": {
			"type": "object",
			"additionalProperties": false,
			"properties": {
				"sender": {
					"description": "Name of category which should trigger an action. Use '$this' to reference the current parameter.",
					"type": "string"
				},
				"value": {
					"description": "Regex for value which should trigger an action.",
					"type": "string"
				}
			},
			"required": [
				"sender",
				"value"
			]
		},
		"Wikidata": {
			"oneOf": [
				{
					"description": "Wikidata property ID.",
					"type": "string"
				},
				{
					"type": "object",
					"additionalProperties": false,
					"properties": {
						"property": {
							"description": "Wikidata property ID.",
							"type": "string"
						},
						"value": {
							"description": "Used when Wikidata API returns an object as result to choose which item to use.",
							"type": "string"
						},
						"filter": {
							"description": "Filter for using a substring of the value returned by Wikidata.",
							"$ref": "#/definitions/WikidataFilter"
						}
					},
					"required": [
						"property"
					]
				}
			]
		},
		"WikidataFilter": {
			"type": "object",
			"additionalProperties": false,
			"properties": {
				"regexp": {
					"description": "RegExp for extracting the wanted substring.",
					"type": "string"
				},
				"group": {
					"description": "Number of group in the defined RegExp to use as result.",
					"type": "integer"
				}
			},
			"required": [
				"regexp",
				"group"
			]
		}
	},
	"properties": {
		"config": {
			"type": "object",
			"properties": {
				"disableeditlinksifpresent": {
					
				},
				"editlinkcontainer": {

				},
				"helpurl": {
					"description": "URL to which Help-button should link to",
					"type": "string"
				},
				"outdatedwarning": {},
				"submitLegal": {},
				"templateToType": {},
				"title": {
					"description": "Title of the dialog window",
					"type": "string"
				},
				"togglevalue_off": {
					"description": "Value to use when a toggle has been switched to off",
					"type": "string"
				},
				"togglevalue_on": {
					"description": "Value to use when a toggle has been switched to on",
					"type": "string"
				}
			}
		},
		"parameters": {
			"type": "object",
			"properties": {
				"categories": {
					"description": "Ordered list of categories which parameters will be assigned to.",
					"type": "array",
					"items": {
						"$ref": "#/definitions/Category"
					}
				},
				"order": {
					"description": "Ordered list of parameters. Each parameter must be defined in a category.",
					"type": "array",
					"items": {
						"type": "string"
					}
				}
			}
		}
	},
	"type": "object"
}